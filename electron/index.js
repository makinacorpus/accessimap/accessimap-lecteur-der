'use strict';

var webspeechapi, DerReader;

const env = require('./env/env.json').env;

var webFrame = require('electron').webFrame;
webFrame.setVisualZoomLevelLimits(1, 1);
webFrame.setLayoutZoomLevelLimits(0, 0);

if (env === 'dev') {
  webspeechapi = require('../modules/tts.webapi/tts.webapi.js');
  DerReader = require('../modules/der-reader/dist/der-reader.js');
} else {
  webspeechapi = require('tts.webapi');
  DerReader = require('der-reader');
}

// Provide the persistentSettings service to TTS service.
webspeechapi.setPersistentSettingsService(DerReader.persistentSettingsService);

// Initialize the audio service with above TTS service.
let audioService = new DerReader.AudioService(webspeechapi);

var remote = require('electron').remote;

// Get the IOD file to load, if any.
const derFilePath = remote.getGlobal('sharedProperties').derFilePath;

// Get the additional screen presets to load, if any.
const additionalScreenPresets = remote.getGlobal('sharedProperties').additionalScreenPresets;

window.nodeRequire = require;
delete window.require;
delete window.exports;
delete window.module;

function exit() {
  var window = remote.getCurrentWindow();
  window.close();
}

DerReader.init({
  container: 'der-reader',
  derFile: derFilePath,
  additionalScreenPresets: additionalScreenPresets,
  audioService: audioService,
  format: 'A3',
  exit: exit
});

window.app = {
  env: env,
  version: process.env.npm_package_version
};
