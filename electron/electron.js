'use strict';

const electron = require('electron');
const fs = require('fs');
const path = require('path');


// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

app.commandLine.appendSwitch('--enable-viewport-meta', 'true');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow () {

  // Create the browser window.
  mainWindow = new BrowserWindow({
    //   width: 1280, height: 800,
      fullscreen: true
  });

  // and load the index.html of the app.
  mainWindow.loadURL('file://' + __dirname + '/index.html');

  // Open the DevTools for debugging purposes.
  // mainWindow.webContents.openDevTools();

  app.commandLine.appendSwitch('enable-speech-dispatcher');

  // This will hold data shared with other processes.
  global.sharedProperties = {};

  // Check whether an IOD file was passed as parameter, to be opened.
  let derFilePath = null;
  if (process.argv.length >= 2) {
    derFilePath = process.argv[1];
  }
  // Send this via a shared variable to the render process.
  global.sharedProperties.derFilePath = derFilePath;

  // Locate an optional file './screen-presets.json': If present,
  // load its content to add it to the App screen presets.
  let additionalScreenPresets = null;
  if (process.argv.length >= 1) {
    const pathToBinaryFile = process.argv[0];
    const applicationPath = path.dirname(pathToBinaryFile);

    try {
      const screenPresetsJson = fs.readFileSync(applicationPath + '/screen-presets.json', 'utf8');
      additionalScreenPresets = JSON.parse(screenPresetsJson);
    }
    catch (err) {
      // Whatever the error (file not found, not readable...), just ignore
      // adding additional screen presets.
    }
  }
  // Send this via a shared variable to the render process.
  global.sharedProperties.additionalScreenPresets = additionalScreenPresets;

  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});
