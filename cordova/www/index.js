function exit() {
    navigator.app.exitApp()
}

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },

    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        // Provide the persistentSettings service to TTS service.
        cordovaTTS.setPersistentSettingsService(window.DerReader.persistentSettingsService);

        // Initialize the audio service with above TTS service.
        audioService = new window.DerReader.AudioService(cordovaTTS);

        window.DerReader.init({
          container: 'der-reader',
          derFile: null,
          additionalScreenPresets: null,
          audioService: audioService,
          format: 'A5',
          exit: exit
        });
    }
};

app.initialize();
