# Accessimap - Lecteur de documents interactifs

![Build Status](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/badges/master/build.svg)

Ce projet fait parti du projet de recherche Accessimap.

Le lecteur de documents interactifs est relié au [projet de l'éditeur](https://gitlab.com/makinacorpus/accessimap-editeur-der).

## Télécharger le lecteur

Pour télécharger la dernière version, merci d'aller à la [page des téléchargements sur le roadmap](http://accessimap-roadmap.surge.sh/lecteur/).

## Contribuer

Pour contribuer, merci de chercher si une issue gitlab n'a pas déjà été ouverte (et/ou fermée).

Dans le cas contraire, ouvrez une issue et/ou proposez une PR/MR.


## Détails du projet

Le Lecteur de documents interactifs (anciennement "DER Reader") est un module pouvant fonctionner sur différentes plateformes.

Il se décline ici avec Cordova et Electron.
Chacun de ces projets utilisent des services spécifiques pour le TTS (Talk To Speech) qui doivent être fournis à l'utilisation du module der-reader.

Pour pouvoir utiliser une base commune entre la version windows et la version mobile, le coeur du projet se trouve dans un module appelé der-reader.

## Développement

Lancer le script du module der-reader
**Note :** Nous conseillons l'utilisation de `nvm` pour que la bonne version de node soit utilisée, comme dans les blocs de commandes qui suivent.
Si vous ne l'avez pas ou ne souhaitez pas l'utiliser, ignorez les commandes `nvm *` et assurez-vous d'utiliser NodeJS 8.

```sh
cd modules/der-reader
nvm use
npm install
npm start
```

**Note:** Si vous rencontrez des erreurs lors de l'installation (par exemple "`npm ERR! Cannot read property 'find' of undefined`"), essayez de lancer la commande suivante puis réessayez la procédure d'installation :

```sh
npm cache verify
```

Dans une console différente, lancer le script pour démarrer electron

```sh
cd electron
nvm use
npm install
npm start
```

## Explications techniques

### modules/der-reader

Le module est utilisé dans electron et cordova pour initialiser le module der-reader avec la configuration adéquate :

```jsx
DerReader.init({
    der: {
        svgFile: './der/carte_avec_source.svg',
        jsonFile: './der/interactions.json'
    }
});

```


### modules/*.tts

Selon les plateformes, cordova a besoin du plugin [cordova-tts](https://github.com/vilic/cordova-plugin-tts) alors que electron sait gérer l'[API HTML Web Speech](https://developer.mozilla.org/en-US/docs/Web/API/Web_Speech_API).

Chaque implémentation de ces services se trouve dans un module intépendant de der-reader, il faut donc les charger, et indiquer le service tts à utiliser en paramètre de l'initialisation du service Audio.


### Electron (Compilation pour Windows)

Builder l'application (pour Windows x64, cf. script npm pour d'autres paramètres/os).

Nécessite wine, cf. .travis.yml pour installer les dépendances.

> **Note** : Pour compiler une version incluant le volet de débug & outils de développement,
> vous devez modifier le fichier `electron/electron.js` et décommenter la ligne suivante :
> <br>
> ```js
> mainWindow.webContents.openDevTools();
> ```

1. Compiler le lecteur.
    ```sh
    cd modules/der-reader
    rm -r node_modules/
    rm -r dist/
    nvm use
    npm install
    npm run build
    ```

1. Compiler l'executable Windows.
    ```sh
    cd ../../electron
    rm -r node_modules/
    rm -r dist/
    nvm use
    yarn install
    yarn build-windows
    ```

Les executables sont générés dans `electron/dist/`:
- `win-ia32-unpacked/` contient la version 32bit.
- `win-unpacked/` contient la version 64bit.

Les 2 versions sont également packagées sous forme de 2 archives ZIP à la racine de `dist`.

### Cordova (Compilation pour Android)

* installer Java JDK 8

    sudo apt-get install openjdk-8-jdk

* La première étape est l'installation du SDK Android sur votre poste (version 6).
    Documentation officielle : https://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html#installing-the-requirements

    Alternativement, vous pouvez vous inspirer de ce qui est fait dans la section `build_android.before_script` du fichier `.gitlab-ci.yml`.

* Installer `cordova` de façon globale.

    ```sh
    npm install -g cordova
    ```

* Ajouter la platforme android.
    Vous devez vous assurer que vous avez le SDK Android 6 d'installé.
    Ensuite :

    ```sh
    cd cordova
    cordova platform add android@6
    ```

A partir de là, votre environnement est prêt à compiler régulièrement le projet pour la plateforme Android. Pour compiler le projet, suivez les instructions suivantes ci-dessous.
Alternativement, vous pouvez vous inspirer de ce qui est fait dans la section `build_android.script` du fichier `.gitlab-ci.yml`.

* Compiler le lecteur.
    ```sh
    cd modules/der-reader
    nvm use
    npm install
    npm run build
    ```

* Compiler l'application Android.
    ```sh
    cd ../../cordova
    rm -r node_modules/
    cd www/
    git clean -Xfd
    cd ../
    nvm use
    npm install
    npm run build
    ```

* Lancer l'application Android.
    Si une tablette Android en mode debug est connectée à votre poste à ce moment, l'application devrait pouvoir être envoyée vers cette dernière et être lancée en exécutant la commande suivante :
    ```sh
    npm run start
    ```
    Notes :
    - Le mode `debug` doit être activé sur la tablette.
    - La tablette doit être éveillée.
    - La tablette ne doit pas être en mode "sécurisé".
    - Si la commande reste bloquée sur `Package name: com.makinacorpus.mapsense` plus de 30 secondes, il faut parfois simplement annuler la commande et la relancer pour que cela fonctionne.

On peut aussi utiliser le mode émulation d'Android.

Créer au préalable un device :
* ouvrir le studio Android
* créer un projet vide (peu importe son paramétrage)
* builder le projet avec succès (j'ai du cliquer sur 'Install build tools 27.0.3 and sync project' pour que ça marche)
* ouvrir le menu `Tools > AVD Manager` (sinon l'option n'apparaît pas)
* appuyer sur 'Create Virtual Device'
* je crée un Nexus 7.02 avec un OS Marshmallow (6, android 23)


### Comment générer automatiquement les builds ?

Ce projet est câblé à Gitlab CI.

A chaque nouveau commit, Gitlab CI vérifiera que le build se construit correctement (web, electron et android).

Pour ajouter ces builds aux releases, il faut créer une nouvelle release en respectant le [Semantic Versioning](http://semver.org/).

Au préalable, il nous paraît utile et important de mettre à jour les versions
des `package.json` des modules `der-reader` (et les autres s'ils ont changé),
des répertoires `electron` et `cordova/www`.
Les builds produits par `electron` disposent de la version npm.

Pour construire la partie Android, de nouveaux scripts npm ont été ajoutés
suite à une mise à jour dans npm 5.
En npm version 5, lorsque l'on a une dépendance 'locale',
`npm install ../le-repertoire-en-question` va créer un lien symbolique
vers ce répertoire directement. Cela nous pose problème lors du `cordova prepare`.
Car le lien symbolique est déplacé tel quel, et pointe sur un répertoire inexistant.
Le script `copy:dependencies` permet de copier les répertoires des
dépendances locales en dur, et ainsi cordova fonctionne mieux.

Une amélioration à venir sera de donner à cordova uniquement le build js
de l'application Android, sans les dépendances `node_modules`.
