// Time (milliseconds) to wait before
// launching the speechSynthesis.speak() function.
const TIMEOUT_FOR_SPEAKING = 100;

/**
 * Default configuration values.
 */
// TTS speed.
const CFG_DEFAULT_SPEED = 1;
// TTS language.
const CFG_DEFAULT_LANGUAGE = 'fr-FR';

const cordovaTTS = {
  speakTimer: null,
  speakEndCallback: null,
  // Dummy minimal configService always returning the provided default value.
  configService: {
    get: (name, defaultValue) => {return defaultValue;}
  },

  setPersistentSettingsService: function setPersistentSettingsService(persistentSettingsService) {
    this.configService = persistentSettingsService;
  },

  _initUtterance: function _initUtterance(text) {
    let utterance = {locale: '', rate: 0, text: ''};
    utterance.locale = this.configService.get('tts:language', CFG_DEFAULT_LANGUAGE);
    utterance.rate = this.configService.get('tts:speed', CFG_DEFAULT_SPEED);
    utterance.text = text;

    return utterance;
  },

  /**
   * Stops any ongoing TTS playback.
   */
  cancel: function cancel() {
    // Clear any potentially running timeout.
    if (this.speakTimer !== null) {
      window.clearInterval(this.speakTimer);
      this.speakTimer !== null

      // If a timeout was waiting for being run, it means
      // that it never got fired. This means the end callback
      // never got a chance to be registered to the TTS service.
      // Hence, fire it ourselves.
      if (typeof this.speakEndCallback === 'function') {
        this.speakEndCallback();
      }

      this.speakEndCallback = null;
    }

    // Cancel any potential in-progress speech.
    window.TTS.speak(
      this._initUtterance(''),
      // TTS.speak() requires a callback, so provide an empty one.
      () => {}
    );
  },

  /**
   * Reads given text with TTS.
   *
   * @param {String} text
   *   The text to synthesize.
   * @param {Function} callback
   *   The callback to be called when the audio playback ends
   *   or gets interrupted.
   */
  speak: function speak(text, callback) {
    this.speakEndCallback = callback;

    // Set a timeout before speaking as calling
    // speak() right after cancel() fails on
    // some device/browser combinations.
    this.speakTimer = window.setTimeout(
      () => {
        this.speakTimer = null;
        this.speakEndCallback = null;
        window.TTS.speak(
          this._initUtterance(text),
          callback
        );
      },
      TIMEOUT_FOR_SPEAKING
    );
  },
};
