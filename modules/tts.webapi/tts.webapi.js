/**
 * Default configuration values.
 */
// TTS speed.
const CFG_DEFAULT_SPEED = 1;
// TTS pitch.
const CFG_DEFAULT_PITCH = 1;
// TTS language.
const CFG_DEFAULT_LANGUAGE = 'fr-FR';

const webspeechapi = {
  speakEndCallback: null,
  // Dummy minimal configService always returning the provided default value.
  configService: {
    get: (name, defaultValue) => {return defaultValue;}
  },

  setPersistentSettingsService(persistentSettingsService) {
    this.configService = persistentSettingsService;
  },

  /**
   * Sorts voices based on their name, to favor Google's ones that
   * sound better.
   * @param {Object} voiceA:
   *   A voice object, as returned by window.speechSynthesis.getVoices().
   * @param {Object} voiceB:
   *   A voice object, as returned by window.speechSynthesis.getVoices().
   * @return {int}
   *   What's expected from a sort function:
   *   -1 if voiceA should be favored over voiceB.
   *   +1 if voiceB should be favored over voiceA.
   *   0 if they are equal.
   */
  _voicesCustomSort(voiceA, voiceB) {
    let voiceAIsGoogle = false;
    if (voiceA.name.search(/Google/i) !== -1) {
      voiceAIsGoogle = true;
    }

    let voiceBIsGoogle = false;
    if (voiceB.name.search(/Google/i) !== -1) {
      voiceBIsGoogle = true;
    }

    if (voiceAIsGoogle && !voiceBIsGoogle) {
      return -1;
    }

    if (!voiceAIsGoogle && voiceBIsGoogle) {
      return 1;
    }

    return 0;
  },

  _initUtterance(text, callback) {
    let utterance = new SpeechSynthesisUtterance();
    let voices = window.speechSynthesis.getVoices();

    // Look for all voices matching our language.
    let langVoices = [];
    for (let i = 0; i < voices.length; i++) {
      if (voices[i].lang.toLowerCase() === CFG_DEFAULT_LANGUAGE.toLowerCase()) {
        langVoices.push(voices[i]);
      }
    }

    // Some matching voices were found, choose one.
    if (langVoices.length) {
      // Sort them according to our custom criteria.
      langVoices.sort(this._voicesCustomSort);

      // Choose the preferred voice.
      utterance.voice = langVoices[0];
      console.log('Using "' + langVoices[0].name + '"')
    }
    else {
      utterance.voiceURI = 'native';
    }

    utterance.volume = 1; // 0 to 1
    utterance.rate = this.configService.get('tts:speed', CFG_DEFAULT_SPEED);
    utterance.pitch = this.configService.get('tts:pitch', CFG_DEFAULT_PITCH);
    utterance.lang = this.configService.get('tts:language', CFG_DEFAULT_LANGUAGE);
    utterance.text = text;

    if (callback) {
      // We have voices: wait until the end of TTS.
      if (voices.length > 0) {
        utterance.onend = () => {
          callback();
        };
      } else {
        // Else trigger callback immediately.
        callback();
      }
    }

    return utterance;
  },

  /**
   * Stops any ongoing TTS playback.
   */
  cancel() {
    // If a timeout was waiting for being run, it means
    // that it never got fired. This means the end callback
    // never got a chance to be registered to the TTS service.
    // Hence, fire it ourselves.
    if (typeof this.speakEndCallback === 'function') {
      this.speakEndCallback();
    }

    this.speakEndCallback = null;

    // Cancel any potential in-progress speech.
    window.speechSynthesis.cancel();
  },

  /**
   * Reads given text with TTS.
   *
   * @param {String} text
   *   The text to synthesize.
   * @param {Function} callback
   *   The callback to be called when the audio playback ends
   *   or gets interrupted.
   */
  speak(text, callback) {
    window.speechSynthesis.speak(
      this._initUtterance(
        text,
        callback
      )
    );
    window.speechSynthesis.resume();
  },
};

module.exports = webspeechapi;
