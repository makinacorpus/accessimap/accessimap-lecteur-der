/* global VERSION COMMIT TAG DATE */
require('!style!css!./Navigation.css');

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PointerInteractions from '../../services/pointerInteractions';
import PersistentSettings from '../../services/persistentSettings';

class Navigation extends Component {
  handleAction = (index) => {
    const currentItem = this.props.items[index];

    // If current item is the special "Back" button, get back.
    if (currentItem.path === 'back') {
      this.context.router.goBack();
    }
    // Otherwise, run corresponding action.
    else {
      this.props.action(index);
    }
  }

  /**
   * Read the current item (defined by index)
   */
  read() {
    if (this.props.items[this.props.index]) {
      let text = this.props.items[this.props.index].name;
      this.props.read(text);
    } else {
      console.warn(`Item n°${this.props.index} doesn't exist`);
    }
  }

  scrollTo(index) {
    if (this.refs['menu-item-' + index]) {
      this.refs['menu-item-' + index].scrollIntoView({
        block: 'center'
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.read && this.props.index !== nextProps.index) {
      this.props.read(nextProps.items[nextProps.index].name);
    }
    if (this.props.index !== nextProps.index) {
      this.scrollTo(nextProps.index)
    }
  }

  componentWillMount() {
    if (!this.props.excludeBackButton) {
      this.props.items.push({path: 'back', name: 'Retour'});
    }
  }

  componentDidMount() {
    if (this.props.items[this.props.index]
        && this.context.router.location.pathname !== '/') {
      this.props.read(this.props.items[this.props.index].name);
      this.scrollTo(this.props.index);
    }

    // Create a new PointerInteractions instance, that will not propagate
    // events to parents so underlaying dom elements do not react when
    // interacting with navigation entries.
    this.pointerInteractions = new PointerInteractions(document.body, PersistentSettings, true);
    this.pointerInteractions.onSwipeUp(() => {
      let newIndex = this.props.index-1;
      if (this.props.index === 0) {
        newIndex = this.props.items.length-1;
      }
      this.props.changeIndex(newIndex);
    });
    this.pointerInteractions.onSwipeDown(() => {
      let newIndex = this.props.index+1;
      if (this.props.index === this.props.items.length-1) {
        newIndex = 0;
      }
      this.props.changeIndex(newIndex);
    });

    this.pointerInteractions.onTap(this.read.bind(this));
    this.pointerInteractions.onDoubleTap(() => { this.handleAction(this.props.index); });

    this.pointerInteractions.run();
  }

  componentWillUnmount() {
    // Remove the 'Back' button if it's there.
    const backButtonIndex = this.props.items.findIndex(item => {
      return item.path === 'back';
    });
    if (backButtonIndex != -1) {
      this.props.items.splice(backButtonIndex, 1);
    }

    this.pointerInteractions.destroy();
  }

  /**
   * Callback when changing the file from the file input.
   */
  onFileInputChange = (e) => {
    if (this.refs.inputfile) {
      let file = this.refs.inputfile.files[0]
      if (file !== undefined) {
        this.props.actions.changeDerFile(file);
        this.props.actions.changeFilter(null);
      } else {
        this.props.options.message('Aucun fichier seléctionné', 'error')
      }
    }
  }

  render() {
    const {items, index} = this.props;
    const content = this.props.content || '';
    return (
      <div id="navigation">
        <div className="modal" ref="mainMenu" id="mainMenu">
          <div className="menu">
            <ul className="selectable-list">
              {
                items &&
                items.map((item, key) => {
                  const isSelected = (key === index) ? 'selected' : '';
                  return (
                    <li key={key} className="selectable-list--item">
                      {
                        item.type === 'file' &&
                        <div>
                          <a className={isSelected} ref={'menu-item-' + key} >{item.name}</a>
                          <input
                            ref="inputfile"
                            id="loadNewFileInput"
                            type="file"
                            className="inputfile"
                            onChange={this.onFileInputChange}
                            style={{display: 'none'}}
                          />
                        </div>
                      }
                      {
                        item.type !== 'file' &&
                        <a className={isSelected} ref={'menu-item-' + key} >{item.name}</a>
                      }
                    </li>
                  );
                })
              }
            </ul>

            {content}
            <p className="version">
              version { VERSION } - { COMMIT } - { TAG }, generated { DATE }
            </p>
          </div>
        </div>
      </div>
    );
  }
}

Navigation.contextTypes = {
  router: PropTypes.object.isRequired
}

export default Navigation
