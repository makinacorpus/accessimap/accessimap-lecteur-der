require('!style!css!./Button.css')
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import PointerInteractions from '../../services/pointerInteractions'
import PersistentSettings from '../../services/persistentSettings';

class Button extends Component{
  constructor(props) {
    super(props)
    this.state = {
      label: this.props.open ? 'Fermer' : this.props.labelClosed
    }
  }

  componentDidMount() {
    const element = document.getElementById(this.props.id);

    // Create a new PointerInteractions instance, that will not propagate
    // events to parents so underlaying dom elements do not react when
    // clicking on the button.
    this.pointerInteractions = new PointerInteractions(element, PersistentSettings, true);
    this.pointerInteractions.onTap(this.handleClick);
    this.pointerInteractions.onDoubleTap(this.handleDoubleClick);
    this.pointerInteractions.run();
  }

  componentWillUnmount() {
    this.pointerInteractions.destroy();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      label: nextProps.open ? 'Fermer' : nextProps.labelClosed
    })
  }

  handleClick = (target) => {
    const {config} = this.props
    let text = this.props.open ? this.props.labelOpened : target.innerText
    config.audioService.speak(text);
  }

  handleDoubleClick = (target) => {
    const {config} = this.props
    this.props.toggleMenu(this.props.id)
  }

  render() {
    return (
      <button
        id={this.props.id}
        type="button"
        className="button fill black"
      >
        {this.state.label}
      </button>
    )
  }
}

Button.contextTypes = {
  router: PropTypes.object.isRequired
}

export default Button