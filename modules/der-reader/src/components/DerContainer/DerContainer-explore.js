import PointerInteractions from '../../services/pointerInteractions';
import PersistentSettings from '../../services/persistentSettings';

const GESTURE_SIMPLE_TAP = 'tap';
const GESTURE_DOUBLE_TAP = 'double_tap';
const GESTURE_LONG_TAP = 'long_tap';

class DerContainerExplore {

  readAudioFileByName = null;
  audioService = null;
  filter = null;
  pois = [];
  actions = {};
  pointerInteractionEvents = [];

  // Each interaction has a unique incremental ID.
  // The latest number is in this variable.
  currentInteractionUID = 0;

  /**
   * Returns an action based on this.filter.id and given gesture.
   *
   * @param {Array | Object} actions
   *   Array of actions to pick one from.
   *   May also be an indivdual action object.
   * @returns {Object}
   *   Returns an action matching given gesture.
   *   If none matches, null is returned.
   */
  _getAction(actions, gesture) {
    if (!Array.isArray(actions) && this.filter) {
      if (actions.filter === this.filter.id && actions.gesture == gesture) {
        return actions;
      }
    } else if (this.filter) {
      return actions.find(currentAction => currentAction.filter === this.filter.id && currentAction.gesture === gesture);
    }

    return null;
  }

  /**
   * Changes fill color of an element to highlight it when
   * it is currently being interracted with.
   * @param {Object} element: The element whose interaction started.
   */
  _onEventStarted = element => {
    // Hilight the object being interacted with.
    element.style.fill = 'red';

    // Reference current interaction number to this element.
    element.interactionUID = this._getNextInteractionUID();
  }

  /**
   * Resets fill color of an element.
   * Used when its interaction ends, to restore its initial state.
   * @param {Object} element: The element whose interaction ended.
   * @param {Number} initialInteractionUID: The initial interaction UID that
   *   was attributed to the interaction when it started.
   */
  _onEventEnded = (element, initialInteractionUID) => {
    // If the initial interaction UID that was attributed does not
    // match current element's interactionUID, it means another one
    // was started in the meantime. In this case, do nothing.
    if (element.interactionUID !== initialInteractionUID) {
      return;
    }

    // Restore object's fill property to its initial state.
    element.style.fill = element.initialFillProperty;
  }

  /**
   * Adds event listeners to DER elements.
   * @param {Object} Parameters. Structure:
   *   {
   *     'readAudioFileByName': {Function},
   *     'ttsSpeak': {Function},
   *     'audioService': {Class},
   *     'filter': {Object},
   *     'pois': {Object},
   *   }
   */
  attachExploreEvents({
    readAudioFileByName,
    ttsSpeak,
    audioService,
    filter,
    pois,
    triggerInteraction,
    removeInteraction,
  }) {
    this.readAudioFileByName = readAudioFileByName;
    this.ttsSpeak = ttsSpeak;
    this.audioService = audioService;
    this.filter = filter;
    this.pois = [];
    this.triggerInteraction = triggerInteraction;
    this.removeInteraction = removeInteraction;
    this.actions = {};
    this.pointerInteractionEvents = [];

    pois.map((poi) => {
      var id = poi.id.split('-').pop();
      var elements = document.querySelectorAll('[data-link="' + id + '"]');

      Object.keys(elements).map((index) => {
        if (elements[index] !== undefined) {
          let currentElement = elements[index];
          currentElement.style.cursor = 'pointer';

          // Save the initial fill property so we can refer to
          // later for restoring the initial state of the element.
          currentElement.initialFillProperty = currentElement.style.fill;

          this.pois.push(currentElement);

          this.actions[id] = poi.actions.action;

          let pointerInteractions = new PointerInteractions(currentElement, PersistentSettings);

          pointerInteractions.onTap((element) => {
            this.initAction(element, GESTURE_SIMPLE_TAP);
          });
          pointerInteractions.onDoubleTap((element) => {
            this.initAction(element, GESTURE_DOUBLE_TAP);
          });
          pointerInteractions.onLongTap((element) => {
            this.initAction(element, GESTURE_LONG_TAP);
          });

          pointerInteractions.run()
          this.pointerInteractionEvents.push(pointerInteractions);
        }
      });
    });
  }

  /**
   * Destroy every PointerInteractionEvents for the current DER
   */
  destroyExploreEvents() {
    this.pointerInteractionEvents.forEach(currentEvent => {
      currentEvent.destroy();
    })
  }

  initAction = (target, gesture) => {
    let id = target.getAttribute('data-link');
    let action = this._getAction(this.actions[id], gesture);

    if (action) {
      this.triggerInteraction(action);
      const currentLinksElement = [].slice.call(document.querySelectorAll(`[data-link="${id}"]`));

      currentLinksElement.forEach(element => {
        this._onEventStarted(element);
      });

      if (action.protocol === 'mp3') {
        this.readAudioFileByName(
          action.value,
          (() => {
            const initialInteractionUID = this.currentInteractionUID;

            return () => {
              currentLinksElement.forEach(element => {
                this._onEventEnded(element, initialInteractionUID);
              });
            };
          })()
        );
      }

      if (action.protocol === 'tts') {
        this.ttsSpeak(
          action.value,
          (() => {
            const initialInteractionUID = this.currentInteractionUID;

            return () => {
              currentLinksElement.forEach(element => {
                this._onEventEnded(element, initialInteractionUID);
              });
            };
          })()
        );
      }
    }
  }

  /**
   * Increments the current unique interaction number and
   * return it.
   */
  _getNextInteractionUID() {
    return ++this.currentInteractionUID;
  }
}

export default new DerContainerExplore()
