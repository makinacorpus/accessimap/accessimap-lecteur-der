require('!style!css!./DerContainer.css')

import React, { Component } from 'react';
import { hashHistory } from 'react-router';

import {
  getFileObject,
  orderFilesByExt,
  parseXml,
  XML2jsobj,
} from '../../services/load-der.js';
import JSZip from 'jszip';

import Explore from './DerContainer-explore.js';
import PointerInteractions from '../../services/pointerInteractions';
import PersistentSettings from '../../services/persistentSettings';
import Button from '../Button/Button.js';

class DerContainer extends Component {
  state = {
    filesByExt: {},
  }

  componentDidMount() {
    this.setDerFile();
  }

  componentWillReceiveProps(nextProps) {
    const oldProps = this.props
    this.props = nextProps

    // Get old and new filter id.
    const oldFilterId = (typeof oldProps.filter === 'object' && oldProps.filter !== null) ? oldProps.filter.id : null;
    const nextFilterId = (typeof nextProps.filter === 'object' && nextProps.filter !== null) ? nextProps.filter.id : null;

    // If file got changed.
    if (oldProps.derFile !== nextProps.derFile) {
      this.setDerFile();
    }
    else if (oldProps.selectedDocument !== nextProps.selectedDocument) {
      this.changeDocument()
    }
    // We simply switched to a different filter: reload them.
    else if (oldFilterId !== nextFilterId) {
      this.setDerActions();
    }
  }

  componentWillUnmount() {
    this._destroyInteractions();
  }

  /**
   * Clears PointerInteraction instances, if any are attached to current DER.
   */
  _destroyInteractions() {
    if (typeof this.filtersPointerInteractions != 'undefined') {
      this.filtersPointerInteractions.destroy();
    }
    if (typeof this.menuPointerInteractions != 'undefined') {
      this.menuPointerInteractions.destroy();
    }
  }

  setDerFile() {
    const {derFile, der} = this.props

    if (der !== null) {
      this.loadDer(der)
    } else {
      if (typeof derFile === 'string') {
        getFileObject(derFile, file => {
          this.openDerFile(file)
        })
      } else {
        this.openDerFile(derFile)
      }
    }
  }

  openDerFile(file) {
    if (!file) {
      this.props.showMessage('Aucun fichier chargé. Veuillez sélectionner un document interactif à l\'aide du menu.', 'error');
      return;
    }

    var new_zip = new JSZip()
    new_zip.loadAsync(file)
    .then(zip => {
      this._extractFiles(zip.files).then((der) => {
        this.props.showMessage('')
        this.props.setDer(der)
        this.playBeep()
        this.loadDer(der)
      }, (error) => {
        this.props.showMessage(error, 'error')
      })
    })
    .catch(err => {
      console.error(err)
      this.props.showMessage('Erreur lors de l\'ouverture du document interactif', 'error')
    })

    // Get back to main page after loading a DER to close the menu.
    setTimeout(() => {
      hashHistory.push('/');
    }, 300);
  }

  /**
   * Reads an MP3 contained in ZIP file, based on its name.
   *
   * @param name: {string} required
   */
  readAudioFileByName = (name) => {
    return new Promise((resolve, reject) => {
      // No Audio service? Reject.
      if (!this.props.audioService) {
        return reject();
      }

      this.props.der.audioFiles[name].async('base64')
        .then(base64string => {
            this.props.audioService.playBase64Audio(
              base64string,
              () => { resolve(); },
            );
          }, function() {
            reject()
          }
        );
    })
  }

  /**
   * Extracts files contained in ZIP file.
   *
   * @param files: {Object} required
   * @param listContainer: {HTMLElement} required
   * @param callback: {Function}
   */
  _extractFiles(files) {
    this.setState({
      filesByExt: orderFilesByExt(files),
    })

    if (this.state.filesByExt.svg.length > 1) {
      this.props.setFilesList(this.state.filesByExt.svg)
    }
    return new Promise((resolve, reject) => {
      this.readFiles(
        this.state.filesByExt.xml[0],
        this.state.filesByExt.svg[this.props.selectedDocument],
        this.state.filesByExt.audioFiles
      ).then(der => {
        resolve(der)
      }, (err) => {
        reject(err)
      })
    })
  }

  readFiles(xml, svg, audioFiles) {
    let der = {
      audioFiles: audioFiles,
    }

    var getJson = new Promise(function(resolve, reject) {
      xml.async('string')
      .then(function(data) {
        var node = parseXml(data);
        var json = XML2jsobj(node.documentElement);

        // Extract filters.
        der['filters'] = [];
        if (! json.filters) {
          var error = 'Problème lors de l\'import du DERi (fichier XML). ';
          console.error(json.body);
          if (json.body && json.body.parsererror) {
            error += json.body.parsererror.div;
          }
          reject(error);
          return;
        }
        if (typeof json.filters.filter != 'undefined') {
          if (Array.isArray(json.filters.filter)) {
            der['filters'] = json.filters;
          }
          else {
            der['filters'] = {filter: [json.filters.filter]};
          }
        }

        // Extract POIs.
        der['pois'] = [];
        if (typeof json.pois.poi != 'undefined') {
          if (Array.isArray(json.pois.poi)) {
            der['pois'] = json.pois;
          }
          else {
            der['pois'] = {poi: [json.pois.poi]};
          }
        }

        resolve();
      }, function(error) {
        reject(error);
      })
    });

    var getSvg = new Promise(function(resolve, reject) {
      svg.async('string')
      .then(function(data) {
        der['svg'] = data
        resolve()
      }, function(error) {
        reject('Problème lors de l\'import du DERi (fichier SVG). ' + error)
      })
    })

    return new Promise((resolve, reject) => {
      Promise.all([getJson, getSvg]).then(function() {
        resolve(der)
      }, function(error) {
        reject(error);
      })
    })
  }

  changeDocument() {
    const {selectedDocument} = this.props
    this.readFiles(this.state.filesByExt.xml[0], this.state.filesByExt.svg[selectedDocument]).then(der => {
      this.props.setDer(der)
      this.loadDer(der)
    }, err => {
      this.props.showMessage(err, 'error')
    })
  }

  /**
   * Loads the DER drawing into the Reader (and effectively into the DOM).
   *
   * @param {Object} der
   *   The DER Object to load.
   *   Expected structure:
   *   {
   *     ...
   *     'pois': {Object},
   *     'svg': {String},
   *     ...
   *   }
   */
  loadDer(der) {
    if (der.svg && der.svg.length) {
      this._destroyInteractions();

      this.refs.container.innerHTML = der.svg;

      // Locate menu and filters placeholders.
      const filtersBtnPlaceholder = document.getElementById('filter-menu-placeholder');
      const menuBtnPlaceholder = document.getElementById('main-menu-placeholder');

      // Handle whether the drawing includes a placeholder for
      // the filters menu button or not.
      if (filtersBtnPlaceholder !== null) {
        // Hide the default 'Filters' button.
        this.props.setDefaultFiltersButtonVisibility(false);

        // Ensure that all the elements the placeholder is made of can
        // receive pointer events so user can interact with it.
        this._recursiveApplyToDOM(filtersBtnPlaceholder, (element) => {
          this._removeStyleProperty(element, 'pointer-events');
        });

        // Add event listeners to DER's filter button.
        this.filtersPointerInteractions = new PointerInteractions(filtersBtnPlaceholder, PersistentSettings);
        this.filtersPointerInteractions.onTap(() => this.read('Filtres'));
        this.filtersPointerInteractions.onDoubleTap(() => this.props.toggleMenu('filters'));

        this.filtersPointerInteractions.run();
      }
      else {
        // Show the default 'Filters' button.
        this.props.setDefaultFiltersButtonVisibility(true);
      }

      // Handle whether the drawing includes a placeholder for
      // the main menu button or not.
      if (menuBtnPlaceholder !== null) {
        // Hide the default 'Menu' button.
        this.props.setDefaultMenuButtonVisibility(false);

        // Ensure that all the elements the placeholder is made of can
        // receive pointer events so user can interact with it.
        this._recursiveApplyToDOM(menuBtnPlaceholder, (element) => {
          this._removeStyleProperty(element, 'pointer-events');
        });

        // Add event listeners to DER's filter button.
        this.menuPointerInteractions = new PointerInteractions(menuBtnPlaceholder, PersistentSettings);
        this.menuPointerInteractions.onTap(() => this.read('Menu'));
        this.menuPointerInteractions.onDoubleTap(() => this.props.toggleMenu('menu'));

        this.menuPointerInteractions.run();
      }
      else {
        // Show the default 'Menu' button.
        this.props.setDefaultMenuButtonVisibility(true);
      }
    }
    else {
      this.props.showMessage('Aucun SVG trouvé', 'error');
    }

    if (der.pois.poi === undefined) {
      this.props.showMessage('Ce document ne contient aucune interaction', 'error');
    }
    else {
      this.setDerActions();
    }
  }

  /**
   * Removes a style property from given DOM element.
   *
   * @param {DOMElement} element
   *   The DOM element to remove a style property from.
   * @param {String} styleName
   *   The name of the style property to remove (e.g. 'color').
   */
  _removeStyleProperty(element, styleName) {
    if (typeof element.style != 'undefined') {
      element.style.removeProperty(styleName);
    }
  }

  /**
   * Recursively walks through a dom element to apply given
   * function to all of the children.
   *
   * @param {DomElement} node
   *   DOM element to apply given function.
   * @param {Function} functionToApply
   *   Function to be called for each DOM element.
   *   This function will receive the DOM element being processed
   *   as parameter.
   */
  _recursiveApplyToDOM(node, functionToApply) {
    functionToApply(node);
    node = node.firstChild;
    while(node) {
      this._recursiveApplyToDOM(node, functionToApply);
      node = node.nextSibling;
    }
  }

  playBeep() {
    let audio = new Audio('./static/c023.ogg')
    audio.volume = .5
    audio.play()
  }

  setDerActions() {
    const {der, ttsSpeak, audioService, showMessage, filter} = this.props
    if (der && der.pois && der.pois.poi) {
      // Explore
      Explore.attachExploreEvents({
        pois: der.pois.poi,
        readAudioFileByName: this.readAudioFileByName,
        ttsSpeak,
        audioService,
        filter,
        showMessage,
        triggerInteraction: this.props.triggerInteraction,
        removeInteraction: this.props.removeInteraction
      })
    }
  }

  /**
   * Reads given string with TTS.
   *
   * @param {String} text
   *   The text to be read by the TTS system.
   */
  read(text) {
    if (this.props.audioService) {
      return this.props.audioService.speak(text);
    }
  }

  render() {
    const {
      config,
      mainMenuIsOpened,
      filtersMenuIsOpened,
      toggleMenu,
      displayContainerButtonFilter,
      displayContainerButtonMenu,
    } = this.props;

    // we display the container only if no menus are displayed
    const containerStyle = {
      display: mainMenuIsOpened || filtersMenuIsOpened ? 'none' : 'block'
    }
    return (
      <div className="der-container-full" id="der-container-full">
        {
          displayContainerButtonMenu &&
          <Button
            id="menu"
            config={config}
            labelClosed="Réglages"
            labelOpened="Fermer le menu"
            open={mainMenuIsOpened}
            toggleMenu={toggleMenu}
          />
        }
        {
          displayContainerButtonFilter &&
          <Button
            id="filters"
            config={config}
            labelClosed="Filtres"
            labelOpened="Fermer les filtres"
            open={filtersMenuIsOpened}
            toggleMenu={toggleMenu}
          />
        }
        <div className="der-container" id="der-container">
          <div
            ref="container"
            style={containerStyle}
          ></div>
        </div>
      </div>
    )
  }
}

export default DerContainer
