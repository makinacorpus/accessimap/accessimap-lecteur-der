import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { hashHistory } from 'react-router';

import DerContainer from './../components/DerContainer/DerContainer.js';
import Message from './../components/Message/Message.js';
import Button from './../components/Button/Button.js';
import Loader from './../components/Loader/Loader.js';

class App extends Component {

  state = {
    openedMenu: '',
    displayDefaultFiltersButton: true,
    displayDefaultMenuButton: true,
    presentationMode: this.props.presentationMode || false,
  };

  componentWillMount() {
    this.props.setOption('audioService', this.props.route.config.audioService);
    this.props.setOption('exit', { fn: this.props.route.config.exit });
    this.props.initConfig({
      format: this.props.route.config.format || this.props.config.format,
    });

    if (
      this.props.derFile === null &&
      this.props.route.config.derFile !== null
    ) {
      this.props.setDerFile(this.props.route.config.derFile);
    }

    // Still no current DER loaded? Redirect to the main menu.
    if (!this.props.derFile) {
      this.context.router.push('/menu');
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.presentationMode != this.props.presentationMode) {
      this.setState({presentationMode: nextProps.presentationMode});
    }
    if (nextProps.routes[nextProps.routes.length - 1].path === 'file') {
      this.context.router.push('/menu');
    }
  }

  /**
   * Toggles the state of a global menu.
   *
   * @param {String} path
   *   Path to the menu to toggle (e.g.: 'menu', 'filters').
   */
  toggleMenu(path) {
    const route = this.props.routes[this.props.routes.length - 1].path;
    const aDerIsLoaded = this.props.der !== null;

    // Menu already open: close it and stop here.
    if (route === path) {
      hashHistory.push('/');
      // We're done.
      return;
    }

    // Opening a menu has to be handled differently for the main settings
    // menu as it should not be openable when we're in presentation mode.
    if (path === 'menu') {
      // Open the main settings menu unless we're in presentation mode.
      // Exception if there are no DER loaded.
      if (!this.state.presentationMode || !aDerIsLoaded) {
        hashHistory.push(path);
        // We're done.
        return;
      }
    }

    // Other menus can be opened freely.
    else {
      hashHistory.push(path);
      // We're done.
      return;
    }
  }

  showMessage = (text, type) => {
    this.props.setMessage({ text: text, type: type });

    // If a message was provided, read it out loud.
    if (text && this.props.config.audioService) {
      return this.props.config.audioService.speak(text);
    }
  }

  changeDerFile(file) {
    if (file !== null) {
      this.props.setDerFile(file);
    } else {
      this.context.router.push('/menu');
    }
  }

  /**
   * Makes the default 'filters' button visible or hidden.
   *
   * @param {Boolean} visible
   *   Set it to true to display the button.
   *   Set it to false to hide it.
   */
  setDefaultFiltersButtonVisibility(visible) {
    this.setState({ displayDefaultFiltersButton: visible });
  }

  /**
   * Makes the default 'menu' button visible or hidden.
   *
   * @param {Boolean} visible
   *   Set it to true to display the button.
   *   Set it to false to hide it.
   */
  setDefaultMenuButtonVisibility(visible) {
    this.setState({ displayDefaultMenuButton: visible });
  }

  /**
   * Reads a text with TTS.
   * Displays its caption if enabled.
   *
   * @param {String} text
   *   Text to read out loud.
   * @param {Function} callback
   *   Function to run once the TTS system is done reading.
   *   Optional.
   */
  ttsSpeak = (text, callback) => {
    if (this.props.config.audioService) {
      this.props.config.audioService.speak(
        text,
        () => {
          if (typeof callback === 'function') {
            callback();
          }

        }
      );
    }
  }

  // Alias for ttsSpeak.
  read = (text, callback) => {
    this.ttsSpeak(text, callback);
  }

  render() {
    const { currentTTSCaption, config, der, selectedDocument, derFile, activeFilter, message, presentationMode } = this.props;
    const pathname = this.props.routing.locationBeforeTransitions.pathname;

    /**
     * Build the navigation menu by injecting actions to this.props.children
     */
    let navigation;
    if (this.props.children) {
      navigation = React.cloneElement(this.props.children, {
        config: config,
        options: {
          der,
          selectedDocument,
          derFile,
          activeFilter,
          presentationMode,
        },
        actions: {
          toggleMenu: this.toggleMenu.bind(this),
          showMessage: (text, type) => this.showMessage(text, type),
          setFilesList: files => this.props.setFilesList(files),
          setDer: der => this.props.setDer(der),
          changeDerFile: file => this.changeDerFile(file),
          changeDocument: this.changeDocument,
          changeFilter: filter => this.props.setFilter(filter),
          setOption: (name, value) => this.props.setOption(name, value),
          setPersistentOption: (name, value) => this.props.setPersistentOption(name, value),
          setPresentationMode: value => this.props.setPresentationMode(value)
        },
      });
    }

    if (this.props.loading || !this.props.config.audioService) {
      return <Loader />;
    }

    const mainMenuIsOpenedOnRoot = pathname === '/menu';
    const mainMenuIsOpened = pathname.indexOf('/menu') === 0;
    const filtersMenuIsOpened = pathname.indexOf('/filters') === 0;
    const aMenuIsOpened = mainMenuIsOpened || filtersMenuIsOpened;

    const aDerIsLoaded = this.props.der !== null;

    const mainContainerClassProp = 'container' +
      (config.displayTTSCaptions === true ? ' displayTTSCaptions' : '') +
      (config.displayMenuRight === true ? ' displayMenuRight' : '') +
      (aMenuIsOpened ? ' in-menu' : '');

    // Display the filters button when:
    // - A DER is loaded and when a menu is opened AND:
    //   * A menu is opened OR
    //   * The default filter button has to be displayed.
    const displayContainerButtonFilter = aDerIsLoaded && (
      aMenuIsOpened || this.state.displayDefaultFiltersButton === true
    );
    const displayContainerButtonMenu = (
      // Otherwise, if a DER is loaded, the button should be visible
      // when a menu is opened, or when the DER instructed App to display it.
      aDerIsLoaded &&
      (aMenuIsOpened || this.state.displayDefaultMenuButton === true)
    );

    return (
      <div className={mainContainerClassProp} ref="app">
        <div className="header">
          <div className="currentTTSCaption">
            { currentTTSCaption }
          </div>
          {
            (
              // Make sure the main menu button is always open in case
              // no menu is opened and no DER is currently loaded.
              (!aDerIsLoaded && !aMenuIsOpened) ||
              // Otherwise, if no DER is loaded and if the menu is not on the root
              // tree, display the menu button as it it used to get back to parent
              // menu trees.
              (!aDerIsLoaded && !mainMenuIsOpenedOnRoot)
            ) &&

            <Button
              id="menu"
              config={config}
              labelClosed="Réglages"
              labelOpened="Fermer le menu"
              open={mainMenuIsOpened}
              toggleMenu={this.toggleMenu.bind(this)}
            />
          }
        </div>
        {
          message && !!message.text &&
          <Message text={message.text} type={message.messageType} />
        }

        <div className="display-area">
          <DerContainer
            setFilesList={files => this.props.setFilesList(files)}
            setDer={der => this.props.setDer(der)}
            der={der}
            selectedDocument={selectedDocument}
            showMessage={this.showMessage}
            ttsSpeak={this.ttsSpeak}
            config={config}
            mainMenuIsOpened={mainMenuIsOpened}
            filtersMenuIsOpened={filtersMenuIsOpened}
            mainMenuIsOpened={mainMenuIsOpened}
            audioService={config.audioService}
            triggerInteraction={this.props.triggerInteraction}
            removeInteraction={this.props.removeInteraction}
            filter={activeFilter}
            derFile={derFile}
            toggleMenu={this.toggleMenu.bind(this)}
            setDefaultFiltersButtonVisibility={this.setDefaultFiltersButtonVisibility.bind(this)}
            setDefaultMenuButtonVisibility={this.setDefaultMenuButtonVisibility.bind(this)}
            setPresentationMode={this.setPresentationMode}
            displayContainerButtonFilter={displayContainerButtonFilter}
            displayContainerButtonMenu={displayContainerButtonMenu}
          />

          <div className="logos">
            <img src="./static/logo-cpv.jpg" />
            <img src="./static/logo-makina.jpg" />
          </div>

        </div>

        {navigation || ''}
      </div>
    );
  }
}

App.contextTypes = {
  router: PropTypes.object.isRequired,
};

export default App;
