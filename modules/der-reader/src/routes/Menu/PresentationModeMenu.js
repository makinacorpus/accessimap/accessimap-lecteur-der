import React, { Component } from 'react'
import PropTypes from 'prop-types'

class PresentationModeMenu extends Component {
  handleClick() {
    // We're not presenting.
    if (!this.props.options.presentationMode) {
      // No DER file currently loaded: Deny the action, and tell the user they
      // should load a DER first.
      if (!this.props.options.derFile) {
        alert('Chargez d\'abord un DER avant de passer en mode présentation.');
        this.context.router.goBack();

        // Abort here.
        return;
      }

      // Ask the user to confirm they are ready to lock the settings menu.
      let presModeToActivate = confirm(
        'Ceci activera le mode "Présentation", ce qui vérouillera immédiatement ' +
        'l\'accès aux réglages de l\'application jusqu\'à sa fermeture. ' +
        'Êtes-vous sûr de vouloir continuer ?'
      );

      // User confirmed: Enable the presentation mode.
      if (presModeToActivate) {
        if (typeof this.props.actions.setPresentationMode === 'function') {
          this.props.actions.setPresentationMode(true);
        }

        this.context.router.push('/');
        return;
      }

      // User cancelled. Go back.
      this.context.router.goBack();
      return;
    }

    // We're already presenting.
    // We should never end up here as the menu is not supposed to be
    // accessible anymore. It can occur if accessing the
    // menu manually via the URL though.
    alert('Vous êtes déjà en mode présentation.');
    this.context.router.goBack();
  }

  componentDidMount() {
    this.refs.presentationModeButton.click()
  }

  render() {
    return (
      <button
        className="btn btn-default"
        ref="presentationModeButton"
        onClick={() => this.handleClick()}>
      </button>
    )
  }
}

PresentationModeMenu.contextTypes = {
  router: PropTypes.object.isRequired,
}

export default PresentationModeMenu
