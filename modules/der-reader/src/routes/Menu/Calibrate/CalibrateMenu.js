import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Navigation from '../../../components/Navigation/Navigation.js'
import CalibrateCanvas from './Calibrate'

const formats = ['A3', 'A4', 'A5', 'A6', 'format19x13', undefined] // Last item is undefined for return button

class CalibrateMenu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      calibrateMode: false,
      totemMarker: null,
      defaultFormat: props.config.format,
      index: formats.indexOf(props.config.format),
    };
  }

  setTotem(o) {
    this.setState({totemMarker: o})
  }

  handleAction = (index) => {

    const format = formats[index];
    let path = this.props.route.childRoutes[index].path

    if (format) {
      const formats = {
        'A3':    {w:42.0/2.54, h:29.7/2.54, name: 'A3'},
        'A4':    {w:29.7/2.54, h:21.0/2.54, name: 'A4'},
        'A5':    {w:21.0/2.54, h:14.8/2.54, name: 'A5'},
        'A6':    {w:14.8/2.54, h:10.5/2.54,  name: 'A6'},
        '19x13': {w:19.0/2.54, h:13.0/2.54, name: 'Format 19x13'},
      };
      const currentFormat = formats[format];
      if (currentFormat) {
        this.setState({calibrateMode: true});
        this.setTotem(currentFormat);
      }
    } else {
      let currentPath = this.context.router.location.pathname;
      this.context.router.push(currentPath + '/' + path);
    }
  }

  changeActiveMenu = (index) => {
    const format = this.props.route.childRoutes[index].format;

    // je ne comprends pas ce que fait ce code, je n'ai pas réussi à passer jusqu'ici
    if (format) {
      this.props.actions.setPersistentOption('format', format);
    } else {
      this.props.actions.setPersistentOption('format', this.state.defaultFormat);
    }

    this.setState({index: index});
  }

  read = (text) => {
    this.props.config.audioService.speak(text);
  }

  render() {
    if (this.state.calibrateMode) {
      return (
        <CalibrateCanvas totemMarker={this.state.totemMarker}></CalibrateCanvas>
      )
    }
    return (
      <Navigation
        action={this.handleAction}
        index={this.state.index}
        items={this.props.route.childRoutes}
        changeIndex={this.changeActiveMenu}
        read={this.read}
        >
      </Navigation>
    )
  }
}

CalibrateMenu.contextTypes = {
  router: PropTypes.object.isRequired,
}

export default CalibrateMenu
