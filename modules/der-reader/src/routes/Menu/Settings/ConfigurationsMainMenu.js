import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Navigation from '../../../components/Navigation/Navigation.js'

/**
 * Parent menu level listing all settings that can be adjusted.
 */
class ConfigurationsMainMenu extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeMenu: 0
    }
  }

  handleAction = () => {
    if (this.props.route.childRoutes[this.state.activeMenu]) {
      let path = this.props.route.childRoutes[this.state.activeMenu].path;

      if (path === 'quit') {
        this.props.config.exit.fn();
      } else if (path === 'file') {
        document.getElementById('loadNewFileInput').click();
      } else {
        let currentPath = this.context.router.location.pathname;
        this.context.router.push(currentPath + '/' + path);
      }
    }
  }

  changeActiveMenu = (index) => {
    this.setState({activeMenu: index})
  }

  read = (text) => {
    this.props.config.audioService.speak(text);
  }

  render() {
    var childrenWithProps;
    if (this.props.children) {
      childrenWithProps = React.cloneElement(this.props.children, {
        options: this.props.options,
        actions: this.props.actions,
        config: this.props.config
      });
    }

    return (
      <div>
        {childrenWithProps ||
          <Navigation
            action={this.handleAction}
            actions={this.props.actions}
            index={this.state.activeMenu}
            items={this.props.route.childRoutes || []}
            changeIndex={this.changeActiveMenu}
            read={this.read}
            >
          </Navigation>
        }
      </div>
    );
  }
}

ConfigurationsMainMenu.contextTypes = {
  router: PropTypes.object.isRequired
}

export default ConfigurationsMainMenu
