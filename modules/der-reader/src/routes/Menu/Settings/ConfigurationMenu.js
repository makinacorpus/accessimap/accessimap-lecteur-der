import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'

import Navigation from '../../../components/Navigation/Navigation.js';
import { setPersistentOption } from '../../../store/actions'
import PersistentSettings from '../../../services/persistentSettings';

/**
 * A menu screen listing all settings available for a configuration.
 * Saves changes locally, in the LocalStorage.
 */
class ConfigurationMenu extends Component {
  constructor(props) {
    super(props)
    const configName = this.props.route.configName;

    const currentOptionValue = PersistentSettings.get(configName, null);
    const defaultOptionValue =this.props.route.configDefaultValue;
    let defaultOptionIndex = 0;
    let initialIndex = this.props.route.configOptions.findIndex((element, index) => {
      if (element.value == defaultOptionValue) {
        defaultOptionIndex = index;
      }
      return element.value == currentOptionValue;
    });

    // If initial option wasn't found in the menu, select the default entry.
    if (initialIndex === -1) {
      initialIndex = defaultOptionIndex;
    }

    this.state = {
      index: initialIndex,
      configName: configName,
    }
  }

  handleAction(index) {
    const optionEntry = this.props.route.configOptions[index];

    if (typeof optionEntry != 'undefined') {
      const optionValue = optionEntry.value;
      const optionLabel = optionEntry.name;

      // Do not save anything if the value is undefined.
      // Happens when user selects entry 'Back' for instance.
      if (typeof optionValue != 'undefined') {
        this.props.setPersistentOption(this.state.configName, optionValue);
      }

      // Feedback confirming selection.
      this.read(
        'Choix "' + optionLabel + '" sauvegardé, retour.',
        () => {
          this.context.router.goBack();
        }
      );
    }
  }

  changeActiveMenu(index) {
    this.setState({index: index})
  }

  read(text, callback) {
    let speechEndCallback = callback;

    if (typeof speechEndCallback == 'undefined') {
      speechEndCallback = () => {};
    }

    this.props.config.audioService.speak(
      text,
      speechEndCallback
    );
  }

  render() {
    return (
      <Navigation
        action={index => this.handleAction(index)}
        index={this.state.index}
        items={this.props.route.configOptions}
        changeIndex={index => this.changeActiveMenu(index)}
        read={text => this.read(text)}
        >
      </Navigation>
    )
  }
}

ConfigurationMenu.contextTypes = {
  router: PropTypes.object.isRequired,
}

const mapDispatchToProps = dispatch => {
  return {
    setPersistentOption: (name, value) => dispatch(setPersistentOption(name, value)),
  }
}

export default connect(null, mapDispatchToProps)(ConfigurationMenu)
