import { connect } from 'react-redux';

import {
  setMessage,
  setDerFile,
  setFilter,
  setFilesList,
  setDer,
  setOption,
  setPersistentOption,
  initConfig,
  setPresentationMode,
} from '../store/actions';
import { actions } from '../store/audio/store';
import App from './App';

const mapStateToProps = (state) => {
  return {
    ...state.appReducer,
    routing: state.routing,
    currentTTSCaption: state.audio.currentTTSCaption
  }
};

const mapDispatchToProps = dispatch => {
  return {
    setMessage: message => dispatch(setMessage(message)),
    setDerFile: file => dispatch(setDerFile(file)),
    setFilter: filter => dispatch(setFilter(filter)),
    setFilesList: files => dispatch(setFilesList(files)),
    setDer: files => dispatch(setDer(files)),
    setOption: (name, value) => dispatch(setOption(name, value)),
    setPersistentOption: (name, value) => dispatch(setPersistentOption(name, value)),
    triggerInteraction: (i) => dispatch(actions.triggerInteraction(i)),
    removeInteraction: (i) => dispatch(actions.removeInteraction(i)),
    initConfig: config => dispatch(initConfig(config)),
    setPresentationMode: bool => dispatch(setPresentationMode(bool)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
