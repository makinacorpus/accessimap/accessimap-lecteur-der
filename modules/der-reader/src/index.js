import PersistentSettings from './services/persistentSettings';
import AudioService from './services/audio';
import webspeechapi from './../../tts.webapi/tts.webapi.js';
import DerReader from './der-reader';


// Provide the persistentSettings service to TTS service.
webspeechapi.setPersistentSettingsService(PersistentSettings);

// Initialize the audio service with above TTS service.
let audioService = new AudioService(webspeechapi);

DerReader.init({
  container: 'der-reader',
  derFile: null,
  additionalScreenPresets: null,
  audioService: audioService,
  format: 'A4',
  // There are no ways to quit the application in Web mode.
  exit: null,
});

function move_handler(ev) {
  ev.preventDefault();
}

const el = document.getElementById('der-reader');
el.addEventListener('touchmove', move_handler, { passive: false });
