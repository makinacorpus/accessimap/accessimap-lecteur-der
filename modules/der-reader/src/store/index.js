import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { routerReducer } from 'react-router-redux';
import thunkMiddleware from 'redux-thunk';

import { screenCalibrate } from '../middlewares/screen';
import localstorage from '../middlewares/localstorage';
import reader from '../middlewares/reader';

import appReducer from './reducers';
import { audioReducer } from './audio/store';

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    })
    : compose;

const store = createStore(
  combineReducers({
    appReducer,
    routing: routerReducer,
    audio: audioReducer,
  }),
  composeEnhancers(
    applyMiddleware(
      localstorage,
      screenCalibrate,
      reader,
      thunkMiddleware
    )
  )
);

export default store;
