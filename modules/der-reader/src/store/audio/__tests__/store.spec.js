import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { actions, audioReducer } from '../store';

const mockStore = configureMockStore([thunk])
const interaction1 = { value: 'pouic1', protocol: 'tts' };
const interaction2 = { value: 'pouic2', protocol: 'tts' };
const initialState = {
  interactions: [ {
    item: interaction1,
  }, {
    item: interaction2,
  }],
  currentTTSCaption: 'pouic1',
}

describe('audio store', () => {
  it('have removeInteraction action', () => {
    expect(actions.removeInteraction).toBeDefined();
  });
  it('have clearInteractions action', () => {
    expect(actions.clearInteractions).toBeDefined();
  });
  it('have triggerInteraction action', () => {
    expect(actions.triggerInteraction).toBeDefined();
  });

  describe('action removeInteraction', () => {
    it('should be created with the action creator', () => {
      const expectedAction = {
        type: 'INTERACTIONS_REMOVE',
        interaction: interaction1,
      };
      expect(actions.removeInteraction(interaction1)).toEqual(expectedAction);
    });

    it('should remove from the store the interaction passed in paramaters', () => {
      const action = {
        type: 'INTERACTIONS_REMOVE',
        interaction: interaction1,
      };

      expect(audioReducer(initialState, action)).toEqual({
        interactions: [ {
          item: interaction2,
        } ],
        currentTTSCaption: '',
      });
    });
  });

  describe('action clearInteractions', () => {
    it('should create actions as much as necessary, depending on how many interactions', () => {

      const expectedActions = [
        { type: 'INTERACTIONS_REMOVE', interaction: interaction1 },
        { type: 'INTERACTIONS_REMOVE', interaction: interaction2 },
        { type: 'SET_TTS_CAPTION', tts: '' },
      ]
      const store = mockStore({ audio: initialState})

      store.dispatch(actions.clearInteractions())
      expect(store.getActions()).toEqual(expectedActions)

    });

    it('should remove all actions from the store', () => {
      let lastState = initialState;
      [
        { type: 'INTERACTIONS_REMOVE', interaction: interaction1 },
        { type: 'INTERACTIONS_REMOVE', interaction: interaction2 },
        { type: 'SET_TTS_CAPTION', tts: '' },
      ].forEach(action => {
        lastState = audioReducer(lastState, action)
      })
      expect(lastState).toEqual({
        interactions: [ ],
        currentTTSCaption: '',
      });
    });
  });

  describe('action triggerInteraction', () => {
    it('should create actions  as much as necessary, depending on how many interactions', () => {

      const expectedActions = [
        { type: 'INTERACTIONS_REMOVE', interaction: interaction1 },
        { type: 'INTERACTIONS_REMOVE', interaction: interaction2 },
        { type: 'SET_TTS_CAPTION', tts: '' },
        { type: 'INTERACTIONS_ADD', interaction: interaction1 },
        { type: 'SET_TTS_CAPTION', tts: interaction1.value },
      ]

      const store = mockStore({ audio: initialState})

      store.dispatch(actions.triggerInteraction(interaction1));
      expect(store.getActions().length).toEqual(5);
      expect(store.getActions()).toEqual(expectedActions);

    });

    it('empty the state from his actions and add the current interaction', () => {
      let lastState = initialState;
      [
        { type: 'INTERACTIONS_REMOVE', interaction: interaction1 },
        { type: 'INTERACTIONS_REMOVE', interaction: interaction2 },
        { type: 'INTERACTIONS_ADD', interaction: interaction1 },
      ].forEach(action => {
        lastState = audioReducer(lastState, action)
      })
      expect(lastState).toEqual({
        interactions: [{
          item: interaction1,
        }],
        currentTTSCaption: '',
      });
    });
  });
});
