import AudioService from '../../services/audio';

const INTERACTIONS_ADD = 'INTERACTIONS_ADD'
const INTERACTIONS_REMOVE = 'INTERACTIONS_REMOVE'
const SET_TTS_CAPTION = 'SET_TTS_CAPTION'

export const actions = {
  removeInteraction: interaction => ({
    type: INTERACTIONS_REMOVE,
    interaction,
  }),
  clearInteractions: () => (dispatch, getState) => {
    getState().audio.interactions.forEach(i => {
      // i.audio.cancel();
      dispatch(actions.removeInteraction(i.item));
    });
    dispatch({
      type: SET_TTS_CAPTION,
      tts: '',
    });
  },
  triggerInteraction: interaction => (dispatch, getState) => {

    dispatch(actions.clearInteractions());

    dispatch({
      type: INTERACTIONS_ADD,
      interaction,
    });

    const tts = interaction.protocol === 'tts' ? interaction.value : 'text unknown (mp3)';

    dispatch({
      type: SET_TTS_CAPTION,
      tts,
    });

  }
}

const defaultState = {
  interactions: [],
  currentTTSCaption: '',
}

export const audioReducer = (state = defaultState, action) => {
  const newState = {
    ...state,
    interactions: state.interactions.map(i => (i)),
  };
  switch(action.type) {
    // remove interaction in payload
    case INTERACTIONS_REMOVE:
      const currentIndex = newState.interactions.findIndex(i => {
        return i.item === action.interaction
      });
      // we splice only if we have an index, else we would delete the first entry...
      if (currentIndex > -1) {
        newState.interactions.splice(currentIndex, 1);
        newState.currentTTSCaption = '';
      }
      break;
    // add a new interaction to be played
    case INTERACTIONS_ADD:
      newState.interactions.push({
        item: action.interaction
      });
      break;
    case SET_TTS_CAPTION:
      newState.currentTTSCaption = action.tts;
      break;
    default:
  }
  return newState;
}
