/** global version */
require('!style!css!./der-reader.css');

import Menu from './routes/Menu/Menu.js';
import Filters from './routes/Filters/Filters.js';

import ConfigurationsMainMenu from './routes/Menu/Settings/ConfigurationsMainMenu';
import ConfigurationMenu from './routes/Menu/Settings/ConfigurationMenu';
import CalibrateMenu from './routes/Menu/Calibrate/CalibrateMenu';
import PresentationModeMenu from './routes/Menu/PresentationModeMenu'

import FastClick from 'fastclick';
import React from 'react';
import ReactDOM from 'react-dom';

import App from './routes/App.container';
import { Provider } from 'react-redux';
import { Router, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import PersistentSettings from './services/persistentSettings';
import AudioService from './services/audio';

import store from './store';

document.addEventListener(
  'deviceready',
  () => {
    // Disable the context menu.
    document.addEventListener('contextmenu', event => event.preventDefault());

    // Disable the back button.
    document.addEventListener('backbutton', event => event.preventDefault(), false);
  },
  false
);

document.addEventListener('contextmenu', event => event.preventDefault());

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(hashHistory, store);

let config = null;

var DerReader = {
  // Make the persistent settings service easily accessible.
  // Mainly used by non-web versions for providing it to the other services.
  persistentSettingsService: PersistentSettings,
  // Same thing for the audio service.
  // Note: With a first letter capitalized as it's a class, and not an initialized instance.
  AudioService: AudioService,

  /**
   * Initialise DER Reader
   * @param {Object} options
   * {
   *     container: {HTMLElement} required
   *     derFile: (zip file path) {string} required
   *     audioService: {Function} required
   *     exit: {Function} required
   * }
   */
  init: function(env_config) {
    config = env_config;
    FastClick.attach(document.body, {});

    // Prepare default screen presets.
    let screenPresets = [
      {value: 96, name: 'Ecran générique 96ppi'},
      {value: 82, name: 'Ecran Iiyama ProLite T2735MSC'},
      {value: 144, name: 'Tablette HTC Google Nexus 9'},
      {value: 127, name: 'Laptop Dell Inspiron 17 7000 series'},
    ];
    // Prepare additional screen presets if any.
    if (typeof config.additionalScreenPresets !== 'undefined' && config.additionalScreenPresets !== null) {
      screenPresets = screenPresets.concat(config.additionalScreenPresets);
    }

    let routes = {
      path: '/',
      component: App,
      config,
      childRoutes: [
        {
          path: 'menu',
          component: Menu,
          name: 'Réglages',
          childRoutes: [
            {
              path: 'configurations',
              component: ConfigurationsMainMenu,
              name: 'Réglages du lecteur',
              childRoutes: [
                {
                  path: 'tts-speed',
                  component: ConfigurationMenu,
                  name: 'Vitesse de diction',
                  configName: 'tts:speed',
                  configDefaultValue: 1,
                  configOptions: [
                    {value: 0.75, name: '75%'},
                    {value: 1, name: '100%'},
                    {value: 1.5, name: '150%'},
                  ],
                },
                {
                  path: 'double-tap-time-frame',
                  component: ConfigurationMenu,
                  name: 'Délai du double tap',
                  configName: 'interactions:taps:double:timeFrame',
                  configDefaultValue: 500,
                  configOptions: [
                    {value: 300, name: '300 millisecondes'},
                    {value: 500, name: '500 millisecondes'},
                    {value: 700, name: '700 millisecondes'},
                  ],
                },
                {
                  path: 'long-tap-time-frame',
                  component: ConfigurationMenu,
                  name: 'Délai du tap long',
                  configName: 'interactions:taps:long:timeFrame',
                  configDefaultValue: 800,
                  configOptions: [
                    {value: 800, name: '800 millisecondes'},
                    {value: 1500, name: '1 seconde et demi'},
                    {value: 2000, name: '2 secondes'},
                  ],
                },
              ],
            },
            {
              path: 'advanced',
              component: ConfigurationsMainMenu,
              name: 'Fonctions avancées',
              childRoutes: [
                {
                  path: 'file',
                  name: 'Charger un nouveau document interactif',
                  type: 'file',
                },
                {
                  path: 'display-tts',
                  component: ConfigurationMenu,
                  name: 'Affichage du surtitre TTS',
                  configName: 'displayTTSCaptions',
                  configDefaultValue: false,
                  configOptions: [
                    {value: false, name: 'Masqué'},
                    {value: true, name: 'Affiché'},
                  ],
                },
                {
                  path: 'display-menu-right',
                  component: ConfigurationMenu,
                  name: 'Affichage du menu sur la droite',
                  configName: 'displayMenuRight',
                  configDefaultValue: false,
                  configOptions: [
                    {value: false, name: 'Désactivé'},
                    {value: true, name: 'Activé'},
                  ],
                },
                {
                  path: 'screen-calibration',
                  component: ConfigurationsMainMenu,
                  name: 'Calibrage de l\'écran',
                  childRoutes: [
                    {
                      path: 'preset',
                      component: ConfigurationMenu,
                      name: 'Choisir une calibration prédéfinie',
                      // This DPI scale is arbitrary, as depending on the OS, browser and other
                      // factors, a "pixel" might not actually be a physical pixel on screen.
                      // This value is used as a scale ratio for displaying a correct size on
                      // screen and will need to be calibrated by hand for each device.
                      // Note: It will rarely match a screen's PPI, unfortunately.
                      configName: 'dpi',
                      configDefaultValue: false,
                      /**
                       * The following is a list of DPI presets.
                       * Each entry is structured as follows:
                       * {
                       *   // DPI for a traget device.
                       *   values: int,
                       *   // Name displayed to the user.
                       *   name: string,
                       * }
                       */
                      configOptions: screenPresets,
                    },
                    {
                      path: 'custom',
                      component: CalibrateMenu,
                      name: 'Calibrage manuel',
                      childRoutes: [
                        { format: 'A3', name: 'Format A3' },
                        { format: 'A4', name: 'Format A4' },
                        { format: 'A5', name: 'Format A5' },
                        { format: 'A6', name: 'Format A6' },
                        { format: '19x13', name: 'Format 19x13' },
                      ],
                    },
                  ],
                },
                {
                  path: 'presentation-mode',
                  component: PresentationModeMenu,
                  name: 'Passer en mode présentation',
                },
                // Placeholder for the QUIT option.
                // Gets replaced further down by either a valid entry,
                // or gets removed if we're on a website.
                {
                  placeholderUID: 'quit',
                },
              ],
            },
          ],
        },
        {
          path: 'filters',
          component: Filters,
          name: 'Filtres',
        },
      ],
    };

    // Include an entry for quitting the application, unless no function
    // for doing so was provided.
    if (typeof config.exit === 'function') {
      replaceRouterPlaceholder(
        routes,
        'quit',
        {
          path: 'quit',
          name: 'Quitter l\'application',
        }
      );
    }
    else {
      replaceRouterPlaceholder(routes, 'quit', null);
    }

    ReactDOM.render(
      <Provider store={store}>
        <Router routes={routes} history={history} />
      </Provider>,
      document.getElementById(config.container)
    );
  },
};

/**
 * Helper replacing a placeholder object within given router tree with a
 * replacement router entry.
 * Note: recursive function.
 *
 * @param {Object|Array} routerTree
 *   The router configuration tree to get modified.
 *   Warning: Gets modified!
 * @param {String} placeholderUID
 *   The unique placehodler identifier of the object to search for.
 * @param {Object|null} replacementRouterEntry
 *   The object that will replace the placeholder object, if found.
 *   If null is given here, the placeholder will be removed insterad of being replaced.
 */
function replaceRouterPlaceholder(routerTree, placeholderUID, replacementRouterEntry) {
  // Search for the placeholder.
  if (Array.isArray(routerTree)) {
    // Loop backward as we're potentially deleting entries.
    for (var i = routerTree.length - 1; i >= 0; i--) {
      let routerEntry = routerTree[i];

      if (routerEntry.placeholderUID && routerEntry.placeholderUID === placeholderUID) {
        // If the replacement value is null, remove the found palceholder.
        if (replacementRouterEntry === null) {
          routerTree.splice(i, 1);
        }
        // Otherwise replace the palcehodler with provided replacement entry.
        else {
          routerTree[i] = replacementRouterEntry;
        }
      }
      else if (routerEntry.childRoutes) {
        replaceRouterPlaceholder(routerEntry.childRoutes, placeholderUID, replacementRouterEntry);
      }
    }
  }
  else if (routerTree !== null && typeof routerTree === 'object' && routerTree.childRoutes) {
    replaceRouterPlaceholder(routerTree.childRoutes, placeholderUID, replacementRouterEntry);
  }
}

module.exports = DerReader;
