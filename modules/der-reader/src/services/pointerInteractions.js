// Set the following constant to true to output debug traces in
// the console.
const ENABLE_CONSOLE_DEBUG = false;

/**
 * Defautl configuration values.
 */
// Time (ms) between two taps that triggers a double tap.
const CFG_DEFAULT_DOUBLE_TAP_TIME = 500;
// Time (ms) of a held tap that triggers a long tap.
// Should be > DOUBLE_TAP_TIME.
const CFG_DEFAULT_LONG_TAP_TIME = 800;
// Minimum number of pixels a pointer interaction needs to "travel" so it
// gets interpreted as a swipe.
const CFG_DEFAULT_SWIPE_MIN_MOVE = 40;

// All events we need to listen to, grouped by global pointer notions.
const EVENT_ALIASES = {
  'pointer-start': ['touchstart', 'mousedown'],
  'pointer-move': ['touchmove', 'mousemove'],
  'pointer-end': ['touchend', 'mouseup'],
};

// List of events that should not be taken into account for the current
// interaction type detection/enforcement.
// In particular: Mouse/touch moves should not enforce anything as they are
// numerous and sometimes triggered at random moments by some browsers/devices
// (when starting the app on the Electron Windows version for instance).
const INTERACTION_CLASS_DETECTION_EVENTS_TO_IGNORE = ['touchmove', 'mousemove'];

/**
 * Handles mouse & touch interactions in a unified fashion.
 * This classes is a simplified version of the notion of "Pointer events",
 * not yet ready for production use when writing this code.
 */
export default class PointerInteractions {
  // Following variables make sure that all events arising during an interaction
  // are from the same class (e.g. MouseEvent, ToucheEvent, etc.).
  // They are static so they are common to all instances of PointerInteractions.
  static currentInteractionClassTimer = null;
  static currentInteractionClass = null;

  /**
   * Triggers callbacks when the following interactions occur on given element:
   * - tap
   * - double tap
   * - long tap
   * - swipes
   *
   * @param {String|HTMLElement} element
   *   String querySelector or HTMLElement.
   *   The element that will react to pointer interactions.
   * @param {Object} persistentSettingsService
   *   The class to be used to retrieve configurations. See class PersistentSettings.
   * @param {Boolean} stopEventsPropagation
   *   Whether events propagation should be stopped.
   *   If the interactions with given element should propagate to underlying dom elements,
   *   set this to false. If they should be stopped, set to true.
   *   Setting this to true should be done with care as it implies that no other
   *   elements will be aware that a pointer event occured on this element.
   *   Optional. Defaults to false (i.e. propagates events).
   */
  constructor(element, persistentSettingsService, stopEventsPropagation = false) {
    // Configuration service.
    this.configService = persistentSettingsService;

    // Event propagation setting.
    this.stopEventsPropagation = stopEventsPropagation;

    // {Object|null}: Initial coordinates when user starts a pointer interaction.
    // Usefull for detecing whether the gesture is a swipe or a tap.
    this.pointerStartPosition = null;
    // {String|null}: Whether current interaction is a swipe or not.
    // May be:
    // - true: Interaction turned out to be a swipe.
    // - false: Interaction turned out not to be a swipe.
    // - null: An interaction is in progress but its type is still unknown.
    this.isASwipe = false;
    // HTMLElement: The target element that will react to pointer interactions.
    this.element = typeof (element) === 'string' ? document.querySelector(element) : element;

    // Following variables hold double and long tap timers when necessary.
    // They are especially usefull for cancelling them when needed.
    this.doubleTapTimer = null;
    this.longTapTimer = null;

    // This particular variable is set to true when the next pointer-end event should be ignored.
    // This occurs when a long tap got triggered: In this case, the next mouseup should
    // not do anything as it is simply the release of what triggered the long tap.
    this.ignoreNextPointerEndEvent = false;
  };

  /**
   * Debug function outputting given string to the console.
   * Works only if ENABLE_CONSOLE_DEBUG constant is set to true.
   *
   * @param {String} string
   */
  dbg = string => {
    if (ENABLE_CONSOLE_DEBUG) {
      const d = new Date();

      var datestring =
        d.getFullYear() + '-' +
        ('0'+(d.getMonth()+1)).slice(-2) + '-' +
        ('0' + d.getDate()).slice(-2) + ' ' +
        ('0' + d.getHours()).slice(-2) + ':' +
        ('0' + d.getMinutes()).slice(-2) + ':' +
        ('0' + d.getSeconds()).slice(-2) + '.' +
        ('0' + d.getMilliseconds()).slice(-3);

      console.log(datestring + ': ' + string);
    }
  };

  /**
   * Clears the double tap timer.
   */
  clearDoubleTapTimer = () => {
    this.dbg('clearDoubleTapTimer()');

    clearTimeout(this.doubleTapTimer);
    this.doubleTapTimer = null;
  };
  /**
   * Clears the long tap timer.
   */
  clearLongTapTimer = () => {
    this.dbg('clearLongTapTimer()');

    clearTimeout(this.longTapTimer);
    this.longTapTimer = null;
  };
  /**
   * Clears all tap timers.
   */
  clearAllTapTimers = () => {
    this.clearDoubleTapTimer();
    this.clearLongTapTimer();
  };
  /**
   * Stops the swipe detection, because another
   * type of interaction was detected before (a long tap for instance).
   * This prevents the PointerMove handler from continuing
   * trying to detect a swipe.
   */
  clearSwipe = () => {
    this.dbg('clearSwipe()');
    this.isASwipe = false;
  };

  /**
   * Triggers the tap action.
   *
   * @param {Event} evt
   *   Original event object that triggered the action.
   */
  triggerTap = evt => {
    this.dbg('>> triggerTap(' + evt.type + ')');

    this.clearAllTapTimers();
    this.clearSwipe();

    if (typeof this.onTap == 'function') {
      this.onTap(evt.target);
    }
  };
  /**
   * Triggers the double tap action.
   *
   * @param {Event} evt
   *   Original event object that triggered the action.
   */
  triggerDoubleTap = evt => {
    this.dbg('>> triggerDoubleTap(' + evt.type + ')');

    this.clearAllTapTimers();
    this.clearSwipe();

    if (typeof this.onDoubleTap == 'function') {
      this.onDoubleTap(evt.target);
    }
  };
  /**
   * Triggers the long tap action.
   *
   * @param {Event} evt
   *   Original event object that triggered the action.
   */
  triggerLongTap = evt => {
    this.dbg('>> triggerLongTap(' + evt.type + ')');

    this.clearAllTapTimers();
    this.clearSwipe();
    this.ignoreNextPointerEndEvent = true;

    if (typeof this.onLongTap == 'function') {
      this.onLongTap(evt.target);
    }
  };
  /**
   * Triggers the swipe UP action.
   *
   * @param {Event} evt
   *   Original event object that triggered the action.
   */
  triggerSwipeUp = evt => {
    this.dbg('>> triggerSwipeUp(' + evt.type + ')');

    this.clearAllTapTimers();
    this.ignoreNextPointerEndEvent = true;

    if (typeof this.onSwipeUp == 'function') {
      this.onSwipeUp(evt.target);
    }
  };
  /**
   * Triggers the swipe DOWN action.
   *
   * @param {Event} evt
   *   Original event object that triggered the action.
   */
  triggerSwipeDown = evt => {
    this.dbg('>> triggerSwipeDown(' + evt.type + ')');

    this.clearAllTapTimers();
    this.ignoreNextPointerEndEvent = true;

    if (typeof this.onSwipeDown == 'function') {
      this.onSwipeDown(evt.target);
    }
  };
  /**
   * Triggers the swipe LEFT action.
   *
   * @param {Event} evt
   *   Original event object that triggered the action.
   */
  triggerSwipeLeft = evt => {
    this.dbg('>> triggerSwipeLeft(' + evt.type + ')');

    this.clearAllTapTimers();
    this.ignoreNextPointerEndEvent = true;

    if (typeof this.onSwipeLeft == 'function') {
      this.onSwipeLeft(evt.target);
    }
  };
  /**
   * Triggers the swipe RIGHT action.
   *
   * @param {Event} evt
   *   Original event object that triggered the action.
   */
  triggerSwipeRight = evt => {
    this.dbg('>> triggerSwipeRight(' + evt.type + ')');

    this.clearAllTapTimers();
    this.ignoreNextPointerEndEvent = true;

    if (typeof this.onSwipeRight == 'function') {
      this.onSwipeRight(evt.target);
    }
  };

  /**
   * Registers the action (callback) to run when the tap
   * interaction gets triggered.
   *
   * @param {Callable} callback
   *   The function to call when this interaction gets triggered.
   *   Will receive the target element as parameter.
   */
  onTap = callback => {
    this.onTap = callback;
    return this;
  };
  /**
   * Registers the action (callback) to run when the double tap
   * interaction gets triggered.
   *
   * @param {Callable} callback
   *   The function to call when this interaction gets triggered.
   *   Will receive the target element as parameter.
   */
  onDoubleTap = callback => {
    this.onDoubleTap = callback;
    return this;
  };
  /**
   * Registers the action (callback) to run when the long tap
   * interaction gets triggered.
   *
   * @param {Callable} callback
   *   The function to call when this interaction gets triggered.
   *   Will receive the target element as parameter.
   */
  onLongTap = callback => {
    this.onLongTap = callback;
    return this;
  };
  /**
   * Registers the action (callback) to run when the swipe UP
   * interaction gets triggered.
   *
   * @param {Callable} callback
   *   The function to call when this interaction gets triggered.
   *   Will receive the target element as parameter.
   */
  onSwipeUp = callback => {
    this.onSwipeUp = callback;
    return this;
  };
  /**
   * Registers the action (callback) to run when the swipe DOWN
   * interaction gets triggered.
   *
   * @param {Callable} callback
   *   The function to call when this interaction gets triggered.
   *   Will receive the target element as parameter.
   */
  onSwipeDown = callback => {
    this.onSwipeDown = callback;
    return this;
  };
  /**
   * Registers the action (callback) to run when the swipe LEFT
   * interaction gets triggered.
   *
   * @param {Callable} callback
   *   The function to call when this interaction gets triggered.
   *   Will receive the target element as parameter.
   */
  onSwipeLeft = callback => {
    this.onSwipeLeft = callback;
    return this;
  };
  /**
   * Registers the action (callback) to run when the swipe RIGHT
   * interaction gets triggered.
   *
   * @param {Callable} callback
   *   The function to call when this interaction gets triggered.
   *   Will receive the target element as parameter.
   */
  onSwipeRight = callback => {
    this.onSwipeRight = callback;
    return this;
  };

  /**
   * Detects whether given event is part of the interaction
   * currently being carried out.
   *
   * This function has been developed to make touch
   * interactions work with different kind of events.
   * Some devices mix Mouse and Touch events in a
   * weird (but standard) order.
   * For instance, on a touch screen, a tap will most
   * likely produce the following events in that order:
   * 1. touchstart 2. touchmove 3. touchend
   * 4. mouseover  5. mousemove 6. mousedown
   * 7. mouseup    8. click
   *
   * To make sure we do not trigger a double tap by detecting
   * both Touch and Mouse events, we take the first event into
   * account (touchstart in this case) and remember its class
   * (TouchEvent) for a short period of time (this.configService.get('interactions:classTimeout'))
   * during which all events of any other class will get ignored
   * (in this example, MouseEvent events would get ignored).
   *
   * This makes possible the use of both touch and mouse events, but
   * not at the same time.
   *
   * This function is the one that registers the first event's
   * class and then rejects or allows all subsequent events
   * depending on whether they match this class or not.
   *
   * @param {Event} evt
   *   The event to check.
   *
   * @returns {Boolean}
   *   Returns true if given event has the same class as the interaction's
   *   currently in progress.
   *   If no interaction was in progress, returns true as this event
   *   will mark the begining of an interaction.
   *   Returns false if given event isn't part of the current
   *   interaction and should be rejected.
   */
  _isEventPartOfcurrentInteractionClass = evt => {
    const givenEventClass = evt.constructor.name;
    const eventType = evt.type;

    // If given event is part of the ones to be ignored, accept it blindly.
    if (INTERACTION_CLASS_DETECTION_EVENTS_TO_IGNORE.indexOf(eventType) > -1) {
      return true;
    }

    // No interaction currently in progress. Accept this event,
    // remember its class and start the interactionClassTimer.
    if (PointerInteractions.currentInteractionClass === null) {
      this.dbg('[Event "' + eventType + '" starts current interaction class: "' + givenEventClass + '"]');

      PointerInteractions.currentInteractionClass = givenEventClass;
      this._startInteractionClassTimer();

      return true;
    }

    // An interaction is currently in progress.
    else {
      // Given event is part of current interaction's class: accept it
      // and restart the interactionClassTimer.
      if (givenEventClass == PointerInteractions.currentInteractionClass) {
        this.dbg('[Event "' + eventType + '" is a "' + PointerInteractions.currentInteractionClass + '": Granted]');

        this._startInteractionClassTimer();
        return true;
      }
      // Given event is not part of current interaction's class: reject it.
      else {
        this.dbg('[Event "' + eventType + '" (of type "' + givenEventClass + '") is not a "' + PointerInteractions.currentInteractionClass + '": Denied]');

        this._startInteractionClassTimer();
        return false;
      }
    }
  };

  /**
   * Helper starting or restarting the interactionClassTimer.
   */
  _startInteractionClassTimer = () => {
    this.dbg('[:: clearTimeout(' + PointerInteractions.currentInteractionClassTimer + ') ::]');
    clearTimeout(PointerInteractions.currentInteractionClassTimer);

    PointerInteractions.currentInteractionClassTimer = setTimeout(
      function _interactionClassTimeout() {
        this.dbg('[:: _interactionClassTimeout() [id=' + PointerInteractions.currentInteractionClassTimer + '] ::]');
        PointerInteractions.currentInteractionClassTimer = null;
        PointerInteractions.currentInteractionClass = null;
      }.bind(this),
      this.getInteractionClassTimeout()
    );

    this.dbg('[:: _startInteractionClassTimer() [id=' + PointerInteractions.currentInteractionClassTimer + '] ::]');
  };

  /**
   * Helper retrieving the target element from
   * given event, using targetTouches if available.
   * See https://developer.mozilla.org/en-US/docs/Web/API/TouchEvent/targetTouches
   */
  _getEventTarget = (evt) => {
    if (evt.targetTouches && evt.targetTouches.length > 0) {
      return evt.targetTouches[evt.targetTouches.length -1];
    }

    return evt;
  };

  /**
   * Handles the start of a pointer interaction which corresponds to a
   * mousedown, a touchstart or any equivalent event that marks
   * the start of a pointer interaction.
   *
   * @param {Event} evt
   *   Original event object that triggered this function.
   */
  handlePointerStartEvent = evt => {
    this.dbg('handlePointerStartEvent(' + evt.type + ')');

    // If propagation should be stopped, stop it.
    if (this.stopEventsPropagation) {
      evt.stopPropagation();
    }

    // If given event is not part of current interaction, ignore it.
    if (!this._isEventPartOfcurrentInteractionClass(evt)) {
      return;
    }

    // When starting a new pointer interaction, this variable is not
    // supposed to be active. If it is, it's most likely because the last pointer-end
    // event occured outside of the scope of the element. So let's reset it.
    this.ignoreNextPointerEndEvent = false;

    // Whenever the user starts a pointer interaction, start the long-tap timer
    // and register a long-tap interaction at the end of it.
    // It will eventually get cancelled if another interaction gets detected before
    // this timeout (like a tap, a swipe, etc.).
    this.longTapTimer = setTimeout(
      this.triggerLongTap,
      this.configService.get('interactions:taps:long:timeFrame', CFG_DEFAULT_LONG_TAP_TIME),
      evt
    );

    // Handle swipes.
    // Save the pointer interaction's starting position.
    let target = this._getEventTarget(evt);
    this.pointerStartPosition = {
      x: target.clientX,
      y: target.clientY,
    };
    // Reset the swipe type as we do not know if it's gonna be a swipe yet.
    this.isASwipe = null;
  };

  /**
   * Handles the movement of an ongoing pointer interaction which corresponds to
   * mousemove, touchmove or any equivalent events that move
   * the position of the current pointer interaction.
   *
   * @param {Event} evt
   *   Original event object that triggered this function.
   */
  handlePointerMoveEvent = evt => {
    this.dbg('handlePointerMoveEvent(' + evt.type + ')');

    // If propagation should be stopped, stop it.
    if (this.stopEventsPropagation) {
      evt.stopPropagation();
    }

    // If given event is not part of current interaction, ignore it.
    if (!this._isEventPartOfcurrentInteractionClass(evt)) {
      return;
    }

    // Detects whether current pointer interaction is a swipe.
    if (this.isASwipe === null) {
      const target = this._getEventTarget(evt);
      const currentPointerPosition = {
        x: target.clientX,
        y: target.clientY,
      };

      const yMove = currentPointerPosition.y - this.pointerStartPosition.y;
      const xMove = currentPointerPosition.x - this.pointerStartPosition.x;

      const swipeMinMove = this.configService.get('interactions:swipes:minMove', CFG_DEFAULT_SWIPE_MIN_MOVE);

      if (yMove <= -swipeMinMove) {
        this.isASwipe = true;
        this.triggerSwipeUp(evt);
      }
      else if (yMove >= swipeMinMove) {
        this.isASwipe = true;
        this.triggerSwipeDown(evt);
      }
      else if (xMove <= -swipeMinMove) {
        this.isASwipe = true;
        this.triggerSwipeLeft(evt);
      }
      else if (xMove >= swipeMinMove) {
        this.isASwipe = true;
        this.triggerSwipeRight(evt);
      }
    }
  };

  /**
   * Handles the end of a pointer interaction which corresponds to a
   * mouseup, a touchend or any equivalent event that marks
   * the end of a pointer interaction.
   *
   * @param {Event} evt
   *   Original event object that triggered this function.
   */
  handlePointerEndEvent = evt => {
    this.dbg('handlePointerEndEvent(' + evt.type + ')');

    // If propagation should be stopped, stop it.
    if (this.stopEventsPropagation) {
      evt.stopPropagation();
    }

    // If given event is not part of current interaction, ignore it.
    if (!this._isEventPartOfcurrentInteractionClass(evt)) {
      return;
    }

    // Any pointer-end event should always cancel the long-tap timer, if running.
    this.clearLongTapTimer();
    // Same goes for the swipe detection.
    this.clearSwipe();

    // Do nothing more if the next pointer-end event had to be ignored.
    if (this.ignoreNextPointerEndEvent) {
      // We're ignoring this pointer-end event, but make sure
      // the next one is taken into account.
      this.ignoreNextPointerEndEvent = false;
      return;
    }

    // If the double-tap timer isn't running, start it
    // just in case the user is about to perform a double-tap.
    // Note that we register a single-tap action at the end of
    // this double-tap timer: Indeed, for now we don't know yet
    // if the interaction is going to be a single or a double tap
    // so we register a single tap by default. The latter will
    // get cancelled if it turns out to be a double-tap.
    if (this.doubleTapTimer === null) {
      this.doubleTapTimer = setTimeout(
        this.triggerTap,
        this.configService.get('interactions:taps:double:timeFrame', CFG_DEFAULT_DOUBLE_TAP_TIME),
        evt
      );
    }
    // Else, the double-tap timer is running: it means that this
    // pointer-end event was made very closely to the previous one,
    // fast enough to trigger a double-tap. Hence, cancel the
    // double-tap timer so it does not trigger the default
    // single-tap registered just above, and trigger the double-tap
    // action instead.
    else {
      this.clearDoubleTapTimer();
      this.triggerDoubleTap(evt);
    }
  };

  /**
   * Helper returning the interaction class timeout which depends on
   * different parameters.
   * @return {Int}
   *   The class timeout to apply, in ms.
   */
  getInteractionClassTimeout = () => {
    let classTimeout = 0;

    const otherTimeouts = [
      this.configService.get('interactions:taps:double:timeFrame', CFG_DEFAULT_DOUBLE_TAP_TIME),
      this.configService.get('interactions:taps:long:timeFrame', CFG_DEFAULT_LONG_TAP_TIME),
    ];

    // The class timeout should always be > than other timeouts.
    // Enforce that by making sure it's just a bit longer than the longest timeout.
    for (let i = 0; i < otherTimeouts.length; i++) {
      if (otherTimeouts[i] > classTimeout) {
        classTimeout = otherTimeouts[i];
      }
    }

    // Return the highest timeout, plus 100ms to be safe.
    return classTimeout + 100;
  }

  /**
   * Starts listening to appropriate events in order to react to pointer interactions.
   */
  run = () => {
    this.dbg('PointerInteractions.run()');

    // Start listening to all events associated to the notion of "pointer-start".
    const pointerStartAliases = EVENT_ALIASES['pointer-start'];
    for (let i = 0; i < pointerStartAliases.length; i++) {
      this.element.addEventListener(pointerStartAliases[i], this.handlePointerStartEvent);
    }

    // Start listening to all events associated to the notion of "pointer-move".
    const pointerMoveAliases = EVENT_ALIASES['pointer-move'];
    for (let i = 0; i < pointerMoveAliases.length; i++) {
      this.element.addEventListener(pointerMoveAliases[i], this.handlePointerMoveEvent);
    }

    // Start listening to all events associated to the notion of "pointer-end".
    const pointerEndAliases = EVENT_ALIASES['pointer-end'];
    for (let i = 0; i < pointerEndAliases.length; i++) {
      this.element.addEventListener(pointerEndAliases[i], this.handlePointerEndEvent);
    }
  };

  /**
   * Stops listening to events.
   */
  destroy = () => {
    this.dbg('PointerInteractions.destroy()');

    // Stop listening to previously registered "pointer-start" events.
    const pointerStartAliases = EVENT_ALIASES['pointer-start'];
    for (let i = 0; i < pointerStartAliases.length; i++) {
      this.element.removeEventListener(pointerStartAliases[i], this.handlePointerStartEvent);
    }

    // Stop listening to previously registered "pointer-move" events.
    const pointerMoveAliases = EVENT_ALIASES['pointer-move'];
    for (let i = 0; i < pointerMoveAliases.length; i++) {
      this.element.removeEventListener(pointerMoveAliases[i], this.handlePointerMoveEvent);
    }

    // Stop listening to previously registered "pointer-end" events.
    const pointerEndAliases = EVENT_ALIASES['pointer-end'];
    for (let i = 0; i < pointerEndAliases.length; i++) {
      this.element.removeEventListener(pointerEndAliases[i], this.handlePointerEndEvent);
    }
  };
}
