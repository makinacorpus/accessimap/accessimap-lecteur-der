import LocalStorage from './localStorage';

/**
 * Handles saving and retrieving DER-Reader persistent settings.
 */
export default class PersistentSettings {
  static get(name, defaultValue) {
    const value = LocalStorage.getItem(name);

    if (value !== null) {
      return value;
    }
    else {
      return defaultValue;
    }
  }

  static getMultiple(namesAndDefaultValues) {
    const names = Object.keys(namesAndDefaultValues);

    let values = LocalStorage.getMultipleItems(names);

    for (let i = 0; i < names.length; i++) {
      let name = names[i];

      if (values[name] === null) {
        values[name] = namesAndDefaultValues[name];
      }
    }

    return values;
  }

  static set(name, value) {
    return LocalStorage.setItem(name, value);
  }
}
