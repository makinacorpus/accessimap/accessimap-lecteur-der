/**
 * Handles TTS and audio playbacks.
 */
export default class AudioService {
  /**
   * Constructor.
   *
   * @param {Object} ttsService
   *   The ready-to-use object handling text-to-speach, already initialized.
   */
  constructor(ttsService) {
    // TTS Service
    this.ttsService = ttsService;

    // Audio resource.
    this.audioResource = null;
  }

  /**
   * Reads given text with TTS.
   * Stops any ongoing TTS/Audio playback.
   *
   * @param {String} text
   *   The text to synthesize.
   * @param {Function} callback
   *   The callback to be called when the audio playback ends
   *   or gets interrupted.
   */
  speak = (text, callback) => {
    this.cancel();
    this.ttsService.speak(text, callback);
  }

  /**
   * Plays given base64 string as an audio resource.
   * Stops any ongoing TTS/Audio playback.
   *
   * @param {String} base64string
   *   The audio resource, encoded in base64.
   * @param {Function} callback
   *   The callback to be called when the audio playback ends
   *   or gets interrupted.
   */
  playBase64Audio = (base64string, callback) => {
    this.cancel();

    this.audioResource = new Audio('data:audio/mpeg;base64,' + base64string)
    this.audioResource.play()
    this.audioResource.onpause = () => {
      callback()
    }
  }

  /**
   * Stops any ongoing TTS/Audio playback.
   */
  cancel = () => {
    // Cancel any potential TTS playback.
    this.ttsService.cancel();

    // Cancel any potential audio resource playback.
    if (this.audioResource !== null) {
      this.audioResource.pause();
    }
  }
}
