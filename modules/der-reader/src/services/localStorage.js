/**
 * Handles saving and retrieving values to/from browsers' localStorage.
 */
export default class LocalStorage {
  static getItem (name) {
    let item;

    try {
      item = JSON.parse(window.localStorage.getItem(name));
    } catch (e) {
      try {
        item = window.localStorage.getItem(name) || null;
      }
      catch (e) {
        item = null;
      }
    }

    return item;
  }

  static getMultipleItems(names){
    if (names instanceof Array) {
      let items = {};

      names.map(name => {
        items[name] = this.getItem(name);
      });

      return items;
    }

    return null
  }

  static setItem(name, value){
    try {
      window.localStorage.setItem(name, JSON.stringify(value));
    }
    catch (e) {
      return false;
    }

    return true;
  }
}
