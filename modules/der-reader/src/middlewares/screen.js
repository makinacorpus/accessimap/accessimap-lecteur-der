import { LOCATION_CHANGE } from 'react-router-redux';
import {
  SET_OPTION,
  INIT_CONFIG,
} from '../store/actions';
import {
  defaultState,
} from '../store/reducers';

/**
 * Middleware for resize application container to fit with DER.
 */
export const screenCalibrate = () => next => action => {
  switch (action.type) {
  case SET_OPTION:
    if (action.name === 'dpi') {
      document.getElementById('der-container').style.transform = 'scale(' + action.value / 96 + ')';
    }

    if (action.name === 'tts') {
      Reader = action.value;
    }
    break;
  case INIT_CONFIG:
    var { format, dpi } = action.value;

    dpi = dpi ? dpi : defaultState.config.dpi;

    // Wait until the "der-container" is loaded, and apply the zoom to it.
    var derContainerObserver = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        // Do nothing if new elements weren't added.
        if (!mutation.addedNodes){
          return;
        }

        var derContainer = document.getElementById('der-container');

        // The "der-container" is loaded: apply the zoom to it.
        if (derContainer !== null) {
          derContainer.style.transform = 'scale(' + dpi / 96 + ')';

          // Stop observing.
          derContainerObserver.disconnect();
        }
      })
    })

    // Start observing.
    derContainerObserver.observe(
      document.body,
      {
        childList: true,
        subtree: true,
        attributes: false,
        characterData: false,
      }
    );

    break;
  default:
    break;
  }
  next(action);
};
