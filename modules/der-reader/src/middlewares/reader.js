/**
 * Middleware for adding TTS on certain route change.
 */
const reader = store => next => action => {
  if (action.type === '@@router/LOCATION_CHANGE') {
    const state = store.getState()

    switch (action.payload.pathname) {
    case '/':
      if (state.appReducer && state.appReducer.config.audioService) {
        const currentRoute = state.routing.locationBeforeTransitions.pathname.slice(1)
        let actionText = ''

        if (currentRoute === 'menu') {
          actionText = 'fermeture du menu'
        }
        if (currentRoute === 'filters') {
          actionText = `Filtre sélectionné : ${state.appReducer.activeFilter.name}.`;
        }

        // Describe action out loud.
        if (actionText) {
          state.appReducer.config.audioService.speak(actionText, () => {});
        }

        // Run next action immediately.
        next(action);
      } else {
        next(action);
      }
      break;
    default:
      next(action);
      break;
    }
  } else {
    next(action);
  }
};

export default reader;