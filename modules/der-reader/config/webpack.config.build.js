var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
/* import postcss config array */
var postCSSConfig = require('./postcss.config');
var commit = process.env.CI_COMMIT_SHA || 'unknown';

module.exports = {
  entry: {
    'der-reader': ['./src/der-reader.js'],
    index: './src/index.js',
  },
  devtool: 'source-map',
  output: {
    path: __dirname + '/../dist/',
    filename: '[name].js',
    library: 'DerReader',
    libraryTarget: 'umd',
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['babel'],
      },
    ],
  },
  postcss: function() {
    return postCSSConfig;
  },
  externals: {
    version: JSON.stringify(require('./../package.json').version),
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.ejs',
      filename: 'index.html',
    }),
    new webpack.DefinePlugin({
      COMMIT: JSON.stringify(commit.substr(0,6)),
      TAG: JSON.stringify(process.env.CI_COMMIT_TAG || 'sans tag gitlab'),
      VERSION: JSON.stringify(require('../package.json').version),
      DATE: JSON.stringify(new Date()),
    }),
  ],
};
